﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace aspnetcore_oidc.Models
{
    public class AppSettings
    {
        private IConfiguration Configuration;
        public AppConfig Config { get { return GetConfig; } }

        public AppSettings()
        {
            Configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile(GetFileName())
                    .Build();
        }

        public String GetFileName()
        {
            String env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (String.IsNullOrEmpty(env))
            {
                return "appsettings.json";
            }
            else
            {
                return String.Format("appsettings.{0}.json", env);
            }
        }

        public AppConfig GetConfig
        {
            get
            {
                return Configuration.Get<AppConfig>();
            }
        }
    }


    public class AppConfig
    {
        public ConfigKeys Keys { get; set; }
    }

    public class ConfigKeys
    {
        public string ProjectView { get; set; }
        public string WebService { get; set; }
        public string APIKey { get; set; }
        public string EncryptionKey { get; set; }
        //--Coupon--Start
        public string UnderAge { get; set; }
        public string MissMatchCPR { get; set; }
        public string FeedbackCoupon { get; set; }
        public string NoneCouponLeft { get; set; }
        public string Error { get; set; }
        //Forbrugsforeningen--start
        public string FUnderAge { get; set; }
        public string FMissMatchCPR { get; set; }
        public string FFeedbackCoupon { get; set; }
        public string FNoneCouponLeft { get; set; }
        public string FError { get; set; }
        //Humac--start
        public string FeedbackHumac { get; set; }
        //Register Customer --Start
        public string RegisterCustomerUnderAge { get; set; }
        public string RegisterCustomerMissMatchCPR { get; set; }
        public string RegisterCustomerFeedback { get; set; }
        public string RegisterCustomerError { get; set; }
        //Retail Customer --Start
        public string RetailCustomerUnderAge { get; set; }
        public string RetailCustomerMissMatchCPR { get; set; }
        public string RetailCustomerFeedback { get; set; }
        public string RetailCustomerError { get; set; }

        // Account 
        public string AccountFeedback { get; set; }

        // EasyGreen
        public string EasyGreenFeedback { get; set; }
        // Api
        public string MinSideApi{ get; set; }
        // AcceptPage
        public string AcceptPageFeedback { get; set; }
        public string ThanksFeedback { get; set; }
        public string AcceptPowerFeedback { get; set; }
        public string PayoutFeedback { get; set; }
    }
}

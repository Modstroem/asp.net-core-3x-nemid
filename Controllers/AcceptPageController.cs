﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json;
using RequestHelperCore;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using aspnetcore_oidc.Models;
using EFS_DAL.Models;
using System.Collections.Generic;

namespace aspnetcore_oidc.Controllers
{
    public class AcceptPageController : Controller
    {
        Models.AppSettings settings = new Models.AppSettings();

        [Authorize]
        public IActionResult Index([FromQuery(Name = "OrderId")] Guid OrderId, [FromQuery(Name = "OrderNumber")] int OrderNumber, [FromQuery(Name = "type")] int type)
        {
            if (User.Identity.IsAuthenticated && HttpContext.Session != null)
            {

                var customerName = User.Claims.Any(x => x.Type.ToLower().StartsWith("name")) ? User.Claims.Where(x => x.Type == "name").Select(x => x.Value).FirstOrDefault() : String.Empty;

                var cprNumber = User.Claims.Where(x => x.Type == "gov:saml:attribute:CprNumberIdentifier").Select(x => x.Value).FirstOrDefault();
                string MinSideApi = settings.Config.Keys.MinSideApi;

                string formattedDate = DateTime.Now.ToString("yyyy-MM-dd");
                var response = RequestHelper.APIPost("Accept", "SaveAcceptData", new Dictionary<string, string>
                {
                    { "customername", customerName},
                    { "cpr", cprNumber},
                    { "date", formattedDate},
                    { "acceptidentifier", OrderId.ToString()},
                    { "ordernumber", OrderNumber.ToString()},
                }, null, MinSideApi);

                if (response.IsSuccessStatusCode)
                {
                    Logout().GetAwaiter().GetResult();
                    //TODO add the endpoint to the place it should redirect to.
                    return Redirect(String.Format($"{settings.Config.Keys.AcceptPageFeedback}?orderid={OrderId}&type={type}"));
                }
                else
                {
                    Logout().GetAwaiter().GetResult();
                    return Redirect(String.Format($"{settings.Config.Keys.Error}"));
                }
                    
            }
            else
            {
                Logout().GetAwaiter().GetResult();
                return null;
            }
            
        }

        public async Task Logout()
        {
            // Call the server to terminate the session
            await HttpContext.SignOutAsync(OpenIdConnectDefaults.AuthenticationScheme);
            // Remove authnetication cookies
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }
    }
}

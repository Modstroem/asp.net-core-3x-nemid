﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;


namespace aspnetcore_oidc.Controllers
{
    public class AcceptPagePowerController : Controller
    {
        Models.AppSettings settings = new Models.AppSettings();

        [Authorize]
        public IActionResult Index([FromQuery(Name = "identifier")] string identifier)
        {
            if (User.Identity.IsAuthenticated && HttpContext.Session != null)
            {

                var customerName = User.Claims.Any(x => x.Type.ToLower().StartsWith("name")) ? User.Claims.Where(x => x.Type == "name").Select(x => x.Value).FirstOrDefault() : String.Empty;
                var validatedCPRNumber = User.Claims.Where(x => x.Type == "gov:saml:attribute:CprNumberIdentifier").Select(x => x.Value).FirstOrDefault();

                return Redirect(String.Format($"{settings.Config.Keys.AcceptPowerFeedback}?identifier={identifier}&newCpr={validatedCPRNumber}&mitIdUsername={customerName}"));

            }
            else
            {
                Logout().GetAwaiter().GetResult();
                return null;
            }
        }

        public async Task Logout()
        {
            // Call the server to terminate the session
            await HttpContext.SignOutAsync(OpenIdConnectDefaults.AuthenticationScheme);
            // Remove authnetication cookies
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using aspnetcore_oidc.Models;
using Microsoft.AspNetCore.Authorization;
using ObjectFactoryCore.Partner;
using EFS_DAL.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http;
using DataTransferObjects.Communication;
using static DataTransferObjects.Enums;
using DataTransferObjects;
using RequestHelperCore;
using DataTransferObjects.OrdreOprettelse;
using System.Net;
using aspnetcore_oidc.Methodes;
using LibraryModstroemLogs;

namespace aspnetcore_oidc.Controllers
{
    public class CampaignCouponController : Controller
    {

        public string coupon = string.Empty;
        public static string emailToRecieveError = "BFE@modstroem.dk";
        int partnerID = 0;
        private int Age = 0;

        private String CityName;
        private String Door;
        private String Floor;
        private String StreetName;
        private String HouseNumber;
        private String ZipCode;
        private String SubCity;
        private String CityMall;
        private String errorMsg;
        private Boolean isCityMall = false;
        private string Adresse = string.Empty;
        private string Cpr = String.Empty;
        private string RegNumber = String.Empty;
        private string AccountNumber = String.Empty;
        private string AltContact = String.Empty;
        private string Phone = String.Empty;
        private DateTime MoveDate = DateTime.MinValue;
        DateTime firstLevDate = DateTime.Now;
        private string PickedDate = string.Empty;
        private DateTime ConvertedDate;
        private string FullName = string.Empty;
        private string customerFirstName = string.Empty;
        private string customerLastName = string.Empty;
        private string Email = string.Empty;
        private string newCustomer = string.Empty;
        private string PartnerName = string.Empty;
        private Boolean IsHumac = false;
        private int SellerID;
        private string CustomerID;
        private string Forbrugsforeningen;
        private int CustomerLbnr;
        private string CommunicationID = string.Empty;
        private string verfiedName = string.Empty;

        private Guid GSA_Basis = new Guid("C6E95B26-F67C-418B-A87A-448DAE20C5DC");
        private Guid Spot10 = new Guid("8F8CCDDF-C382-44BF-A1EA-8073C7AE5BA7");
        Models.AppSettings settings = new Models.AppSettings();

        // Authorize telling the framework to only allow requests from authenticated users and if not already authenticated this kicks off the process
        [Authorize] 
        public IActionResult Index([FromQuery(Name = "partnerId")] string PartnerID, [FromQuery(Name = "adresse")] string adresse, [FromQuery(Name = "streetname")] string streetname, [FromQuery(Name = "housenumber")] string housenumber, [FromQuery(Name = "floor")] string floor, [FromQuery(Name = "door")] string door, [FromQuery(Name = "subcity")] string subcity, [FromQuery(Name = "zipcode")] string zipcode, [FromQuery(Name = "cityname")] string cityname, [FromQuery(Name = "name")] string name, [FromQuery(Name = "cPR")] string cPR, [FromQuery(Name = "email")] string email, [FromQuery(Name = "aEgtefaelleSamlever")] string altContact, [FromQuery(Name = "regNr")] string regNr, [FromQuery(Name = "kontonummer")] string kontonummer, [FromQuery(Name = "telefonnummer")] string phone, [FromQuery(Name = "forbrugsforeningen")] string forbrugsforeningen, [FromQuery(Name = "vaelgIndflytningsdato")] string indflytningsDate, [FromQuery(Name = "storcenter")] string storcenter, [FromQuery(Name = "humac")] string humac, [FromQuery(Name = "communicationID")] string communicationID)

        {
            try
            {
                if (User.Identity.IsAuthenticated && HttpContext.Session != null)
                {
                    // Retrieve age claim from user and parse it to an integer
                    var age = User.Claims.Any(x => x.Type.ToLower().StartsWith("age")) ? User.Claims.Where(x => x.Type == "age").Select(x => x.Value).FirstOrDefault() : String.Empty;
                    Int32.TryParse(age, out Age);

                    // Retrieve whether the user is a youth from claims
                    var isYouth = User.Claims.Any(x => x.Type.StartsWith("gov:saml:attribute:IsYouthCert")) ? User.Claims.Select(x => x.Type.StartsWith("gov:saml:attribute:IsYouthCert")).FirstOrDefault().ToString().ToLower() : String.Empty;
                    //var nAge = User.Claims.Where(x => x.Type == "gov:saml:attribute:IsYouthCert").Select(x => x.Value).FirstOrDefault();

                    // Retrieve CPR number from claims
                    var cprk = User.Claims.Where(x => x.Type == "gov:saml:attribute:CprNumberIdentifier").Select(x => x.Value).FirstOrDefault();
                    // Goverment verfied name
                    verfiedName = User.Claims.Where(x => x.Type == "name").Select(x => x.Value).FirstOrDefault();

                    if (string.IsNullOrEmpty(cPR))
                    {
                        Logout().GetAwaiter().GetResult();
                        return Redirect($"{settings.Config.Keys.Error}");
                    }

                    if (string.IsNullOrEmpty(cprk))
                    {
                        Logout().GetAwaiter().GetResult();
                        return Redirect($"{settings.Config.Keys.Error}");
                    }

                    // Remove dashes from CPR numbers for comparison
                    string noDashCprFromQuery = GeneralMethodes.RemoveCprDash(cPR);
                    string noDashCprMitID = GeneralMethodes.RemoveCprDash(cprk);
                    // Check if user is old enough
                    bool isOldEnough = GeneralMethodes.ValidAge(Age, isYouth);

                    // Check if CPR numbers match
                    if (isOldEnough)
                    {
                        if (noDashCprFromQuery == noDashCprMitID)
                        {
                            Cpr = cprk;
                        }
                        else
                        {
                            // mismatch cpr so logout the authenticated user and create log.
                            Logout().GetAwaiter().GetResult();
                            GeneralMethodes.CreateLogMessage($"mismatch with the cpr:{cprk} and {cPR}", "MitID -> CampaignCoupon", 2);
                            return Redirect($"{settings.Config.Keys.MissMatchCPR}");
                        }
                    }
                    else
                    {
                        // under the age of 18 so logout the authenticated user.
                        Logout().GetAwaiter().GetResult();
                        GeneralMethodes.CreateLogMessage($"under the age of 18 with the cpr:{cprk}", "MitID -> CampaignCoupon", 2);
                        return Redirect($"{settings.Config.Keys.UnderAge}");
                    }

                    // Set session variables with information from query
                    Int32.TryParse(PartnerID, out partnerID);
                    HttpContext.Session.SetInt32("partnerId", partnerID);
                    PickedDate = indflytningsDate;
                    Forbrugsforeningen = forbrugsforeningen;
                    Adresse = adresse;
                    FullName = name;
                    Email = email;
                    AltContact = altContact;
                    RegNumber = regNr;
                    AccountNumber = kontonummer;
                    Phone = phone;
                    StreetName = streetname;
                    HouseNumber = housenumber;
                    Floor = floor;
                    Door = door;
                    SubCity = subcity;
                    ZipCode = zipcode;
                    CityName = cityname;
                    CityMall = storcenter;
                    CommunicationID = communicationID;
                    IsHumac = !string.IsNullOrEmpty(humac);

                    if (IsHumac)
                    {
                        SellerID = 123605;
                    }
                    if (partnerID <= 0)
                    {
                        Logout().GetAwaiter().GetResult();
                        GeneralMethodes.CreateLogMessage($"partnerID: {partnerID} is lower or equal to 0", "MitID -> CampaignCoupon", 2);
                        return Redirect($"{settings.Config.Keys.NoneCouponLeft}");
                    }

                    if (!string.IsNullOrEmpty(CityMall))
                    {
                        isCityMall = true;
                    }

                    if (!string.IsNullOrEmpty(PickedDate))
                    {
                        ConvertedDate = Convert.ToDateTime(PickedDate);
                    }

                    // Split FullName into first and last name
                    if (!string.IsNullOrEmpty(FullName))
                    {
                        var names = GeneralMethodes.SplitFullName(FullName);
                        customerFirstName = names.Item1;
                        customerLastName = names.Item2;
                    }

                }
                else
                {
                    // User is not authenticated, log the error
                    GeneralMethodes.CreateLogMessage("(0): User is not authenticated", "MitID -> CampaignCoupon", 2);
                }
                return Protected();
            }
            catch (Exception e)
            {
                // Log exception details
                GeneralMethodes.CreateLogMessage(e.Message, "MitID -> CampaignCoupon", 3);
                GeneralMethodes.CreateLogMessage(e.StackTrace, "MitID -> CampaignCoupon", 3);
                return null;
            }
        }
        // The [Authorize] is a way of telling the framework to only allow requests from authenticated users.
        [Authorize]
        public IActionResult Protected()

        {
            try
            {
                // authenticated user allowed and reirected with coupon.
                if (User.Identity.IsAuthenticated && HttpContext.Session != null)
                {
                    // If not a CityMall partner, get the coupon and partner details
                    if (!isCityMall)
                    {
                        var tupleItems = CouponMethodes.GetCoupon(partnerID);
                        coupon = tupleItems.Item1;
                        PartnerName = tupleItems.Item2;
                        SellerID = tupleItems.Item3;
                    }
                    else
                    {
                        // If it is a CityMall partner, get the partner details
                        DefCampaignPartner mitIDPartner = CampaignPartner.GetPartner(partnerID);

                        if (mitIDPartner != null)
                        {
                            PartnerName = mitIDPartner.PartnerName;
                            SellerID = mitIDPartner.SellerId;
                        }
                        else
                        {
                            GeneralMethodes.CreateLogMessage("(1) partnerid: " + partnerID + " = null", "MitID -> CampaignCoupon", 2);
                        }
                    }

                    //Check if there is a coupon available
                    if (!string.IsNullOrEmpty(coupon))
                    {
                        // Create the customer
                        CreateCustomer();

                        // If there is no error message, redirect the user to the appropriate page
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            GeneralMethodes.CreateLogMessage("(2): coupon findes: " + coupon, "MitID -> CampaignCoupon", 2);
                            //CampaignPartner.UpdateCouponAvailable(availableCoupon, 0); //sets the coupon to 0 (used)

                            //Logout the authenticated user.
                            Logout().GetAwaiter().GetResult();

                            if (IsHumac)
                            {
                                GeneralMethodes.CreateLogMessage($"Redirect the user: {CustomerLbnr} to the Humac feedback page with the coupon: {coupon} and partnerID: {partnerID}", "MitID -> CampaignCoupon", 2);

                                // Redirect the user to the Forbrugsforeningen feedback page with the coupon and partner ID
                                return Redirect(string.Format($"{settings.Config.Keys.FeedbackHumac}?partnerid={partnerID}&coupon={coupon}"));
                            } 
                            //forbrugsforeningen
                            else if (!string.IsNullOrEmpty(CustomerID) && !string.IsNullOrEmpty(Forbrugsforeningen))
                            {
                                GeneralMethodes.CreateLogMessage($"Redirect the user: {CustomerLbnr} to the Forbrugsforeningen: {Forbrugsforeningen} feedback page with the coupon: {coupon} and partnerID: {partnerID}", "MitID -> CampaignCoupon", 2);

                                // Redirect the user to the Forbrugsforeningen feedback page with the coupon and partner ID
                                return Redirect(string.Format($"{settings.Config.Keys.FFeedbackCoupon}?partnerid={partnerID}&forbrugsforeningen={CustomerID}"));
                            }
                            else
                            {
                                GeneralMethodes.CreateLogMessage($"Redirect the user: {CustomerLbnr} to the partner feedback page with the coupon: {coupon} and partnerID: {partnerID}", "MitID -> CampaignCoupon", 2);

                                // Redirect the user to the feedback page with the coupon and partner ID
                                return Redirect(string.Format($"{settings.Config.Keys.FeedbackCoupon}?partnerid={partnerID}"));
                            }
                        }
                        else
                        {
                            GeneralMethodes.CreateLogMessage($"(2.1): error: {errorMsg}", "MitID -> CampaignCoupon", 2);

                            //Logout the authenticated user and redirect to error page.
                            Logout().GetAwaiter().GetResult();
                            return Redirect($"{settings.Config.Keys.Error}");
                        }
                    }
                    else if (isCityMall)
                    {
                        //Create customer and check if this is city mall partner.
                        CreateCustomer();
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            //Log the success and redirection
                            GeneralMethodes.CreateLogMessage("(3): success and redirection.", "MitID -> CampaignCoupon", 2);
                            //Logout the authenticated user.
                            Logout().GetAwaiter().GetResult();
                            //Redirect to feedback coupon page with the city mall and partner id
                            return Redirect(string.Format($"{settings.Config.Keys.FeedbackCoupon}?storcenter={CityMall}&partnerid={partnerID}"));//redirect with the coupon and partnerID.
                        }
                    }
                    else 
                    {
                        GeneralMethodes.CreateLogMessage($"no coupon left for this partner: {partnerID}", "MitID -> CampaignCoupon", 2);

                        //Logout the authenticated user.
                        Logout().GetAwaiter().GetResult();
                        //Redirect to no coupon left page
                        return Redirect($"{settings.Config.Keys.NoneCouponLeft}");
                    }

                }

                return View();
            }
            catch (Exception e)
            {
                GeneralMethodes.CreateLogMessage(e.Message, "MitID -> CampaignCoupon", 3);
                GeneralMethodes.CreateLogMessage(e.StackTrace, "MitID -> CampaignCoupon", 3);
                return null;
            }
        }

        public async Task Logout()
        {
            // Call the server to terminate the session
            await HttpContext.SignOutAsync(OpenIdConnectDefaults.AuthenticationScheme);
            // Remove authnetication cookies
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return Redirect($"{settings.Config.Keys.Error}");
            //return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public void CreateCustomer()
        {
            try
            {
                // Check if the current day is before the 18th of the month
                if (DateTime.Now.Day < 18)
                {
                    // If it is, set the firstLevDate to the 1st day of the next month + 2 months
                    firstLevDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(2);
                }
                else
                {
                    // else, set the firstLevDate to the 1st day of the next month + 3 months
                    firstLevDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(3);
                }

                // Create a new list of CustomerContractPropertyDTO objects
                List<DataTransferObjects.OrdreOprettelse.CustomerContractPropertyDTO> contractProp = new List<CustomerContractPropertyDTO>();

                // Add a new CustomerContractPropertyDTO object to the list
                contractProp.Add(new CustomerContractPropertyDTO
                {
                    IdContract = new Guid(),
                    IdAlias = (int)DataTransferObjects.Enums.ContractPropertyAlias.ContractLength,
                    Value = "6"
                });

                int betalingsID = 0;
                if (Forbrugsforeningen == "true")
                {
                    betalingsID = (int)Enums.PaymentForm.Betalingskort;
                }
                else //Gavekort
                {
                    betalingsID = (int)Enums.PaymentForm.PBS;
                }

                // Initialize a new instance of the CustomerDTO class with values from the query.
                CustomerDTO customer = new CustomerDTO
                {
                    CPRNummer = String.IsNullOrEmpty(Cpr) ? String.Empty : Cpr,
                    AdresseValideret = true,
                    AlternativKontaktperson = String.IsNullOrEmpty(AltContact) ? String.Empty : AltContact,
                    Bynavn = String.IsNullOrEmpty(CityName) ? String.Empty : CityName,
                    DoerBetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                    Efternavn = String.IsNullOrEmpty(customerLastName) ? String.Empty : customerLastName,
                    Fornavn = String.IsNullOrEmpty(customerFirstName) ? String.Empty : customerFirstName,
                    Etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                    Email = String.IsNullOrEmpty(Email) ? String.Empty : Email,
                    Gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                    Husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                    Postnummer = String.IsNullOrEmpty(ZipCode) ? String.Empty : ZipCode,
                    Telefon = String.IsNullOrEmpty(Phone) ? String.Empty : Phone,
                    Mobil = String.IsNullOrEmpty(Phone) ? String.Empty : Phone,
                    VPRNummer = String.Empty,
                    Firmanavn = String.Empty,
                    FirmakontaktPerson = String.Empty,
                    Stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                    Kundetype = (int)Enums.Segment.Private,
                    servicepakke = null,
                    BetalingsID = betalingsID,
                    Registreringsnummer = RegNumber,
                    Kontonummer = AccountNumber,
                    relation = String.IsNullOrEmpty(AltContact) ? "Ingen" : "Samlever",
                    ReferralID = String.Empty,
                    Transaktionsnummer = String.Empty,
                };

                if (string.IsNullOrEmpty(PickedDate))
                {
                    // Create a new list of product DTOs for the customer.
                    customer.produkter = new List<ProduktDTO>()
                    {
                        new ProduktDTO
                        {
                            bynavn = CityName,
                            doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                            etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                            gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                            husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                            gratis_stroem_om_natten = false,
                            aftagenummer = String.Empty,
                            AdresseValideret = true,
                            amr = false,
                            binding = 6,
                            kontrakttype = (int)ContractTypes.el,
                            loft = false,
                            postnummer = ZipCode,
                            stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                            tillaeg = 10,
                            sparegaranti = true,
                            Partner =  new Guid("90036C08-F41A-4D98-95D6-F36D969E7AE6"),
                            Sælger = SellerID, // her skal ændres
                            opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                            Modul = OprettelsesModul.Unset,
                            valideringsStatus = ValideringsStatus.Registreret,
                            internalID = Guid.NewGuid(),
                            produkt_id = Spot10 // Spot + 10 øre
                        },
                        new ProduktDTO
                        {
                            bynavn = CityName,
                            doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                            etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                            gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                            husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                            gratis_stroem_om_natten = false,
                            aftagenummer = String.Empty,
                            AdresseValideret = true,
                            amr = false,
                            binding = 6,
                            kontrakttype = (int)ContractTypes.savingsAgreement,
                            loft = false,
                            postnummer = ZipCode,
                            stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                            tillaeg = 0,
                            sparegaranti = true,
                            Partner = new Guid("90036C08-F41A-4D98-95D6-F36D969E7AE6"),
                            Sælger = SellerID, // her skal SellerID bruges
                            opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                            Modul = OprettelsesModul.Unset,
                            valideringsStatus = ValideringsStatus.Registreret,
                            internalID = Guid.NewGuid(),
                            produkt_id = GSA_Basis, // Grøn Spareaftale (Basis)
                            ContractProperties = contractProp

                        },
                    };
                    customer.FaktureringsForm = new DataTransferObjects.OrdreOprettelse1.InvoicingMethodDTO
                    {
                        invoicingMethod = DataTransferObjects.OrdreOprettelse1.InvoicingMethod.invoice_by_mail,
                        PrimaryEmail = Email,
                        Apartment = String.Empty,
                        Attention = String.Empty,
                        CareOf = String.Empty,
                        Cityname = String.Empty,
                        Country = String.Empty,
                        Floor = String.Empty,
                        Housenumber = String.Empty,
                        SecondaryEmail = String.Empty,
                        Streetname = String.Empty,
                        Subcity = String.Empty,
                        Zipcode = String.Empty
                    };
                    customer.RewardData = new RewardDataDTO
                    {
                        CustomerNumber = 0,
                        ReferenceNumber = PartnerName,
                        ContractNumber = 0,
                        RewardAmount = 1,
                        Origin = Origin.partner_portal,
                        Reward = RewardType.New_Customer,
                        CalculationType = RewardCalculationType.Value,
                        Start_Date = DateTime.Now,
                        Stop_Date = new DateTime(2025, 12, 31),
                        PartnerName = "Campaign"
                    };
                }
                else
                {
                    customer.produkter = new List<ProduktDTO>()
                    {
                        new ProduktDTO
                        {
                            bynavn = CityName,
                            doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                            etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                            gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                            husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                            gratis_stroem_om_natten = false,
                            aftagenummer = String.Empty,
                            AdresseValideret = true,
                            amr = false,
                            binding = 6,
                            kontrakttype = (int)ContractTypes.el,
                            loft = false,
                            postnummer = ZipCode,
                            stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                            tillaeg = 10,
                            sparegaranti = true,
                            Partner =  new Guid("90036C08-F41A-4D98-95D6-F36D969E7AE6"),
                            Sælger = SellerID, // her skal ændres
                            opstartsdato = ConvertedDate,
                            Modul = OprettelsesModul.Unset,
                            valideringsStatus = ValideringsStatus.Registreret,
                            internalID = Guid.NewGuid(),
                            produkt_id = Spot10 // Spot + 10 øre
                        },
                        new ProduktDTO
                        {
                            bynavn = CityName,
                            doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                            etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                            gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                            husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                            gratis_stroem_om_natten = false,
                            aftagenummer = String.Empty,
                            AdresseValideret = true,
                            amr = false,
                            binding = 6,
                            kontrakttype = (int)ContractTypes.savingsAgreement,
                            loft = false,
                            postnummer = ZipCode,
                            stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                            tillaeg = 0,
                            sparegaranti = true,
                            Partner = new Guid("90036C08-F41A-4D98-95D6-F36D969E7AE6"),
                            Sælger = SellerID, // her skal SellerID bruges
                            opstartsdato = ConvertedDate,
                            Modul = OprettelsesModul.Unset,
                            valideringsStatus = ValideringsStatus.Registreret,
                            internalID = Guid.NewGuid(),
                            produkt_id = GSA_Basis, // Grøn Spareaftale (Basis)
                            ContractProperties = contractProp

                        },
                    };
                    customer.FaktureringsForm = new DataTransferObjects.OrdreOprettelse1.InvoicingMethodDTO
                    {
                        invoicingMethod = DataTransferObjects.OrdreOprettelse1.InvoicingMethod.invoice_by_mail,
                        PrimaryEmail = Email,
                        Apartment = String.Empty,
                        Attention = String.Empty,
                        CareOf = String.Empty,
                        Cityname = String.Empty,
                        Country = String.Empty,
                        Floor = String.Empty,
                        Housenumber = String.Empty,
                        SecondaryEmail = String.Empty,
                        Streetname = String.Empty,
                        Subcity = String.Empty,
                        Zipcode = String.Empty
                    };
                    customer.RewardData = new RewardDataDTO
                    {
                        CustomerNumber = 0,
                        ReferenceNumber = PartnerName,
                        ContractNumber = 0,
                        RewardAmount = 1,
                        Origin = Origin.partner_portal,
                        Reward = RewardType.New_Customer,
                        CalculationType = RewardCalculationType.Value,
                        Start_Date = ConvertedDate,
                        Stop_Date = new DateTime(2025, 12, 31),
                        PartnerName = "Campaign"
                    };
                }

                // Creation of the customer.
                HttpResponseMessage result = RequestHelper.APIPost("CustomerController", "CreatePreapprovedCustomer", null, new StringContent(JsonConvert.SerializeObject(customer).ToString(), Encoding.UTF8, "application/json"));

                //If the request was successful
                if (result.IsSuccessStatusCode)
                {
                    // deserialize the response content to a customer object.
                    DataTransferObjects.OrdreOprettelse.CustomerDTO customerCreated = JsonConvert.DeserializeObject<DataTransferObjects.OrdreOprettelse.CustomerDTO>(result.Content.ReadAsStringAsync().Result);

                    //store the customer ID
                    CustomerID = customerCreated.KundeID.ToString();

                    if (!isCityMall)
                    {
                        ObjectFactoryCore.Partner.CampaignPartner.LogCouponCustomer(customerCreated, coupon);
                    }

                    //store the customer number
                    CustomerLbnr = customerCreated.Kundenummer;
                    //create a communication log
                    CreateCommunicationLog(CommunicationID, customerCreated);
                    //send welcome mail
                    SendWelcomeMail(customerCreated, Enums.DocumentType.Velkomstmail_Partner_portal);
                    //create a ticket
                    CreateTicket(customerCreated.Kundenummer);
                }
                else
                {
                    // Assign the content of the HTTP response to the error message variable
                    errorMsg = result.Content.ReadAsStringAsync().Result;
                    // Log the error message veriable if customer creation fails
                    GeneralMethodes.CreateLogMessage($"Couldn't create customer: {errorMsg}", "Modstroem.dk -> CampaignCoupon => MitID", 2);
                    // Send an email with error message 
                    GeneralMethodes.SendEmail(errorMsg, emailToRecieveError, "Partner Campaign", "CampaignCoupon - Mit & NemID", "Partner Portal Status");
                }
            }
            catch (Exception e)
            {
                GeneralMethodes.CreateLogMessage($"Customer creation failed. {e.Message}", "MitID", 3);
                GeneralMethodes.CreateLogMessage($"Customer creation failed. {e.StackTrace}", "MitID", 3);
            }
        }

        private void CreateTicket(int customernumber)
        {
            try
            {
                String ticketContent = String.Empty;
                String ticketTitle = String.Format("Gavekort - "+ PartnerName);
                if (!isCityMall)
                {
                    ticketContent = String.Format("Gavekortkode: " + coupon);
                }
                else
                {
                    ticketContent = String.Format("Gavekortkoden er modtaget i: " + CityMall);
                }
                TicketInfo ticket = new TicketInfo
                {
                    Customernumber = customernumber,
                    Category = (int)DataTransferObjects.Enums.TicketCategory.Andet,
                    Status = (int)DataTransferObjects.Enums.TicketStatus.CLOSED,
                    Department = (int)DataTransferObjects.Enums.TicketDepartment.Sales,
                    Priority = (int)DataTransferObjects.Enums.TicketPriority.Medium,
                    Responsible = (int)DataTransferObjects.Enums.TicketResponsible.Salgssupport,
                    Title = ticketTitle,
                    Description = ticketContent
                };

                var response = RequestHelper.APIPost("InternalController", "CreateTicket", null, new StringContent(JsonConvert.SerializeObject(ticket).ToString(), Encoding.UTF8, "application/json"));

                if (!response.IsSuccessStatusCode)
                {
                    LibraryModstroemLogs.Logging.CreateMessageLog($"Ticket could not be created. Content: {ticketContent}", "Modstroem.dk => CampaignCoupon => MitID => CreateTicket", (int)Enums.MessageLogType.Fejl);
                }
            }
            catch (Exception ex)
            {
                LibraryModstroemLogs.Logging.CreateMessageLog($"Exception: {ex.Message} - {ex.StackTrace}", "Modstroem.dk => CampaignCoupon => MitID => CreateTicket", (int)Enums.MessageLogType.Exception);
            }
        }

        protected Boolean SendWelcomeMail(CustomerDTO Customer, Enums.DocumentType DocType)
        {
            DateTime opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate;
            try
            {
                List<QueueItem> queueItems = new List<QueueItem>();
                if (string.IsNullOrEmpty(PickedDate))
                {
                    AdditionelInfo addInfo = new AdditionelInfo
                    {
                        ticketInfo = new TicketInfo
                        {
                            Customernumber = Customer.Kundenummer,
                            Status = (int)TicketStatus.CLOSED,
                            Category = (int)TicketCategory.Andet,
                            Department = (int)TicketDepartment.Kunde,
                            Priority = (int)TicketPriority.Medium,
                            Responsible = 0
                        },
                        mergeData = new Dictionary<string, string>
                    {
                        {"[GAVEKORT-KODE]", $"{coupon}" },
                        {"[Forhandler]", $"{PartnerName}"},
                        {"[Leverancestart]", $"{opstartsdato.ToShortDateString()}"},
                    },

                    };

                    queueItems.Add(new QueueItem
                    {
                        SenderID = 2,
                        AddedBy = "CampaignFormFlow",
                        CreateTicket = true,
                        Instant = true,
                        customerNumber = Customer.Kundenummer,
                        ContractLbnr = Customer.produkter.First().Kontraktnummer,
                        Trackable = false,
                        documenttype = DocType,
                        documentFormat = Enums.DocumentFormat.HTML,
                        procesType = ProcessingType.Email,
                        additionelInfo = addInfo,
                    });
                }
                else
                {
                    AdditionelInfo addInfo = new AdditionelInfo
                    {
                        ticketInfo = new TicketInfo
                        {
                            Customernumber = Customer.Kundenummer,
                            Status = (int)TicketStatus.CLOSED,
                            Category = (int)TicketCategory.Andet,
                            Department = (int)TicketDepartment.Kunde,
                            Priority = (int)TicketPriority.Medium,
                            Responsible = 0
                        },
                        mergeData = new Dictionary<string, string>
                    {
                        {"[GAVEKORT-KODE]", $"{coupon}" },
                        {"[Forhandler]", $"{PartnerName}"},
                        {"[Leverancestart]", $"{ConvertedDate.ToShortDateString()}"},
                    },

                    };

                    queueItems.Add(new QueueItem
                    {
                        SenderID = 2,
                        AddedBy = "CampaignFormFlow",
                        CreateTicket = true,
                        Instant = true,
                        customerNumber = Customer.Kundenummer,
                        ContractLbnr = Customer.produkter.First().Kontraktnummer,
                        Trackable = false,
                        documenttype = DocType,
                        documentFormat = Enums.DocumentFormat.HTML,
                        procesType = ProcessingType.Email,
                        additionelInfo = addInfo,
                    });
                }
                

                var content = new StringContent(JsonConvert.SerializeObject(queueItems).ToString(), Encoding.UTF8, "application/json");
                var response = RequestHelper.APIPost("NotificationController", "SetDocumentsInQueue", null, content);

                if (response.IsSuccessStatusCode)
                {
                    GeneralMethodes.CreateLogMessage($"Success to send Welcome Email for customer - return = true", "MitID -> CampaignCoupon", 2);
                    return true;
                }
                else
                {
                    GeneralMethodes.CreateLogMessage($"Failed to send Welcome Email for customer - return = false", "MitID -> CampaignCoupon", 1);
                    return false;
                }
            }
            catch (Exception e)
            {
                GeneralMethodes.CreateLogMessage($"Failed to send Welcome Email for customer - {e.Message}", "MitID -> CampaignCoupon", 3);
                GeneralMethodes.CreateLogMessage($"Failed to send Welcome Email for customer - {e.StackTrace}", "MitID -> CampaignCoupon", 3);
                return false;
            }
        }

        protected void CreateCommunicationLog(string communicationID, CustomerDTO customer)
        {
            try
            {
                LibraryModstroemLogs.Logging.UpdateCustomerLBNR(communicationID, customer.Kundenummer.ToString());

                string ip = string.Empty;
                IPAddress myIP = HttpContext.Connection.RemoteIpAddress;

                DataTransferObjects.Internal.AcceptIdentityDTO logInfo = new DataTransferObjects.Internal.AcceptIdentityDTO();
                logInfo.Content = String.Empty;
                logInfo.Customer_Lbnr = customer.Kundenummer.ToString();
                logInfo.Origin_URL = "CampaignFormFlow";
                logInfo.AcceptpageType = 1;

                try
                {
                    IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
                    logInfo.Origin_IP = GetIPHost.HostName.ToString();
                    ip = GetIPHost.HostName.ToString();
                    GeneralMethodes.GetCommunicationID(ip, customer, verfiedName);
                }
                catch
                {
                    logInfo.Origin_IP = myIP.ToString();
                }

                StringContent content = new StringContent(JsonConvert.SerializeObject(logInfo).ToString(), Encoding.UTF8, "application/json");
                HttpResponseMessage result = RequestHelper.APIPost("IdentityController", "RegisterAcceptpageCommunication", null, content);

                if (result.IsSuccessStatusCode)
                {
                    GeneralMethodes.CreateLogMessage($"Communication was successfully logged for customer: {customer.Kundenummer}", "MitID -> CampaignCoupon", 2);
                }

                else
                {
                    GeneralMethodes.CreateLogMessage($"Communication logging failed for customernumer {customer.Kundenummer} - {result.Content.ReadAsStringAsync().Result}", "MitID -> CampaignCoupon", 1);
                }

            }
            catch (Exception e)
            {
                GeneralMethodes.CreateLogMessage($"Communication logging failed for customernumer {customer.Kundenummer} - {e.Message} - {e.StackTrace}", "MitID -> CampaignCoupon", 3);
            }
        }
    }
}

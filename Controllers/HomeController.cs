﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using aspnetcore_oidc.Models;
using Microsoft.AspNetCore.Authorization;
using ObjectFactoryCore.Partner;
using EFS_DAL.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http;
using DataTransferObjects.Communication;
using static DataTransferObjects.Enums;
using DataTransferObjects;
using RequestHelperCore;
using DataTransferObjects.OrdreOprettelse;
using System.Net;
using aspnetcore_oidc.Methodes;

namespace aspnetcore_oidc.Controllers
{
    public class HomeController : Controller
    {

        public string coupon = string.Empty;
        public static string emailToRecieveError = "rto@modstroem.dk";
        int partnerID = 0;
        private int Age = 0;

        private String CityName;
        private String Door;
        private String Floor;
        private String StreetName;
        private String HouseNumber;
        private String ZipCode;
        private String SubCity;

        private String errorMsg;

        private string Adresse = string.Empty;
        private string Cpr = String.Empty;
        private string RegNumber = String.Empty;
        private string AccountNumber = String.Empty;
        private string AltContact = String.Empty;
        private string Phone = String.Empty;
        private DateTime MoveDate = DateTime.MinValue;
        DateTime firstLevDate = DateTime.Now;
        private string PickedDate = string.Empty;
        private DateTime ConvertedDate;
        private string FullName = string.Empty;
        private string customerFirstName = string.Empty;
        private string customerLastName = string.Empty;
        private string Email = string.Empty;
        private string newCustomer = string.Empty;
        private string PartnerName = string.Empty;

        private int SellerID;
        private string CustomerID;
        private string Forbrugsforeningen;
        private int CustomerLbnr;
        Models.AppSettings settings = new Models.AppSettings();


        [Authorize] //is a way of telling the framework to only allow requests from authenticated users and if not already authenticated this kicks off the process
        public IActionResult Index([FromQuery(Name = "partnerId")] string PartnerID, [FromQuery(Name = "adresse")] string adresse, [FromQuery(Name = "streetname")] string streetname, [FromQuery(Name = "housenumber")] string housenumber, [FromQuery(Name = "floor")] string floor, [FromQuery(Name = "door")] string door, [FromQuery(Name = "subcity")] string subcity, [FromQuery(Name = "zipcode")] string zipcode, [FromQuery(Name = "cityname")] string cityname, [FromQuery(Name = "name")] string name, [FromQuery(Name = "cPR")] string cPR, [FromQuery(Name = "email")] string email, [FromQuery(Name = "aEgtefaelleSamlever")] string altContact, [FromQuery(Name = "regNr")] string regNr, [FromQuery(Name = "kontonummer")] string kontonummer, [FromQuery(Name = "telefonnummer")] string phone, [FromQuery(Name = "forbrugsforeningen")] string forbrugsforeningen, [FromQuery(Name = "vaelgIndflytningsdato")] string indflytningsDate)

        {

            if (User.Identity.IsAuthenticated && HttpContext.Session != null)
            {
                var age = User.Claims.Any(x => x.Type.ToLower().StartsWith("age")) ? User.Claims.Where(x => x.Type == "age").Select(x => x.Value).FirstOrDefault() : String.Empty;
                Int32.TryParse(age, out Age);
                var isYouth = User.Claims.Any(x => x.Type.StartsWith("gov:saml:attribute:IsYouthCert")) ? User.Claims.Select(x => x.Type.StartsWith("gov:saml:attribute:IsYouthCert")).FirstOrDefault().ToString().ToLower() : String.Empty;
                //var nAge = User.Claims.Where(x => x.Type == "gov:saml:attribute:IsYouthCert").Select(x => x.Value).FirstOrDefault();
                var cprk = User.Claims.Where(x => x.Type == "gov:saml:attribute:CprNumberIdentifier").Select(x => x.Value).FirstOrDefault();

                if (string.IsNullOrEmpty(cPR))
                {
                    Logout().GetAwaiter().GetResult();
                    return Redirect($"{settings.Config.Keys.Error}");
                }

                if (string.IsNullOrEmpty(cprk))
                {
                    Logout().GetAwaiter().GetResult();
                    return Redirect($"{settings.Config.Keys.Error}");
                }

                string noDashCprFromQuery = GeneralMethodes.RemoveCprDash(cPR);
                string noDashCprMitID = GeneralMethodes.RemoveCprDash(cprk);
                bool isOldEnough = GeneralMethodes.ValidAge(Age, isYouth);

                if (isOldEnough)
                {
                    if (noDashCprFromQuery == noDashCprMitID)
                    {
                        Cpr = cprk;
                    }
                    else 
                    {
                        // mismatch cpr
                        Logout().GetAwaiter().GetResult();//Logout the authenticated user.
                        return Redirect($"{settings.Config.Keys.MissMatchCPR}");
                    }
                }
                else 
                {
                    // under the age of 18
                    Logout().GetAwaiter().GetResult();//Logout the authenticated user.
                    return Redirect($"{settings.Config.Keys.UnderAge}");
                }

                Int32.TryParse(PartnerID, out partnerID);
                HttpContext.Session.SetInt32("partnerId", partnerID);
                PickedDate = indflytningsDate;
                Forbrugsforeningen = forbrugsforeningen;
                Adresse = adresse;
                FullName = name;
                Email = email;
                AltContact = altContact;
                RegNumber = regNr;
                AccountNumber = kontonummer;
                Phone = phone;
                StreetName = streetname;
                HouseNumber = housenumber;
                Floor = floor;
                Door = door;
                SubCity = subcity;
                ZipCode = zipcode;
                CityName = cityname;

                if (!string.IsNullOrEmpty(PickedDate))
                {
                    ConvertedDate = Convert.ToDateTime(PickedDate);
                }


                if (!string.IsNullOrEmpty(FullName))
                {
                    var names = GeneralMethodes.SplitFullName(FullName);
                    customerFirstName = names.Item1;
                    customerLastName = names.Item2;
                }

            }
            return Protected();
        }
        // The [Authorize] is a way of telling the framework to only allow requests from authenticated users.
        [Authorize] // If not already authenticated this kicks off the process
        public IActionResult Protected()

        {   // authenticated user allowed and reirected with coupon.
            if (User.Identity.IsAuthenticated && HttpContext.Session != null)
            {
                //int partnerID = (int)HttpContext.Session.GetInt32("partnerId");
                var tupleItems = CouponMethodes.GetCoupon(partnerID);
                coupon = tupleItems.Item1;
                PartnerName = tupleItems.Item2;
                SellerID = tupleItems.Item3;    
                //if there is a coupon
                if (!string.IsNullOrEmpty(coupon))
                {
                    CreateCustomer();//Creates the customer

                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        //CampaignPartner.UpdateCouponAvailable(availableCoupon, 0);//sets the coupon to 0 (used)
                        Logout().GetAwaiter().GetResult();//Logout the authenticated user.
                        //forbrugsforeningen
                        if (!string.IsNullOrEmpty(CustomerID) && !string.IsNullOrEmpty(Forbrugsforeningen))
                        {
                            return Redirect(string.Format($"{settings.Config.Keys.FFeedbackCoupon}?coupon={coupon}&partnerid={partnerID}&forbrugsforeningen={CustomerID}"));
                        }
                        else
                        {
                            return Redirect(string.Format($"{settings.Config.Keys.FeedbackCoupon}?coupon={coupon}&partnerid={partnerID}"));//redirect with the coupon and partnerID.
                        }
                    }
                    else
                    {
                        Logout().GetAwaiter().GetResult();//Logout the authenticated user.
                        return Redirect($"{settings.Config.Keys.Error}");
                    }
                }
                else // no coupon left page redirection
                {
                    Logout().GetAwaiter().GetResult();//Logout the authenticated user.
                    return Redirect($"{settings.Config.Keys.NoneCouponLeft}");
                }

            }

            return View();
        }

        public async Task Logout()
        {
            // Call the server to terminate the session
            await HttpContext.SignOutAsync(OpenIdConnectDefaults.AuthenticationScheme);
            // Remove authnetication cookies
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return Redirect($"{settings.Config.Keys.Error}");
            //return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public void CreateCustomer()
        {
            try
            {
                if (DateTime.Now.Day < 18)
                {
                    firstLevDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(2);
                }
                else
                {
                    firstLevDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(3);
                }
                List<DataTransferObjects.OrdreOprettelse.CustomerContractPropertyDTO> contractProp = new List<CustomerContractPropertyDTO>();
                contractProp.Add(new CustomerContractPropertyDTO
                {
                    IdContract = new Guid(),
                    IdAlias = (int)DataTransferObjects.Enums.ContractPropertyAlias.ContractLength,
                    Value = "6"
                });

                CustomerDTO customer = new CustomerDTO
                {
                    CPRNummer = String.IsNullOrEmpty(Cpr) ? String.Empty : Cpr,
                    AdresseValideret = true,
                    AlternativKontaktperson = String.IsNullOrEmpty(AltContact) ? String.Empty : AltContact,
                    Bynavn = String.IsNullOrEmpty(CityName) ? String.Empty : CityName,
                    DoerBetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                    Efternavn = String.IsNullOrEmpty(customerLastName) ? String.Empty : customerLastName,
                    Fornavn = String.IsNullOrEmpty(customerFirstName) ? String.Empty : customerFirstName,
                    Etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                    Email = String.IsNullOrEmpty(Email) ? String.Empty : Email,
                    Gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                    Husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                    Postnummer = String.IsNullOrEmpty(ZipCode) ? String.Empty : ZipCode,
                    Telefon = String.IsNullOrEmpty(Phone) ? String.Empty : Phone,
                    Mobil = String.IsNullOrEmpty(Phone) ? String.Empty : Phone,
                    VPRNummer = String.Empty,
                    Firmanavn = String.Empty,
                    FirmakontaktPerson = String.Empty,
                    Stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                    Kundetype = (int)Enums.Segment.Private,
                    servicepakke = null,
                    BetalingsID = (int)Enums.PaymentForm.PBS,
                    Registreringsnummer = RegNumber,
                    Kontonummer = AccountNumber,
                    relation = String.IsNullOrEmpty(AltContact) ? "Ingen" : "Samlever",
                    ReferralID = String.Empty,
                    Transaktionsnummer = String.Empty,
                };
                if (string.IsNullOrEmpty(PickedDate))
                {
                    customer.produkter = new List<ProduktDTO>()
                    {
                        new ProduktDTO
                        {
                            bynavn = CityName,
                            doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                            etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                            gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                            husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                            gratis_stroem_om_natten = false,
                            aftagenummer = String.Empty,
                            AdresseValideret = true,
                            amr = false,
                            binding = 6,
                            kontrakttype = (int)ContractTypes.el,
                            loft = false,
                            postnummer = ZipCode,
                            stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                            tillaeg = 10,
                            sparegaranti = true,
                            Partner =  new Guid("90036C08-F41A-4D98-95D6-F36D969E7AE6"),
                            Sælger = SellerID, // her skal ændres
                            opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                            Modul = OprettelsesModul.Unset,
                            valideringsStatus = ValideringsStatus.Registreret,
                            internalID = Guid.NewGuid(),
                            produkt_id = new Guid("8F8CCDDF-C382-44BF-A1EA-8073C7AE5BA7")
                        },
                        new ProduktDTO
                        {
                            bynavn = CityName,
                            doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                            etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                            gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                            husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                            gratis_stroem_om_natten = false,
                            aftagenummer = String.Empty,
                            AdresseValideret = true,
                            amr = false,
                            binding = 6,
                            kontrakttype = (int)ContractTypes.savingsAgreement,
                            loft = false,
                            postnummer = ZipCode,
                            stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                            tillaeg = 0,
                            sparegaranti = true,
                            Partner = new Guid("90036C08-F41A-4D98-95D6-F36D969E7AE6"),
                            Sælger = SellerID, // her skal SellerID bruges
                            opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                            Modul = OprettelsesModul.Unset,
                            valideringsStatus = ValideringsStatus.Registreret,
                            internalID = Guid.NewGuid(),
                            produkt_id = new Guid("C6E95B26-F67C-418B-A87A-448DAE20C5DC"),
                            ContractProperties = contractProp

                        },
                    };
                    customer.FaktureringsForm = new DataTransferObjects.OrdreOprettelse1.InvoicingMethodDTO
                    {
                        invoicingMethod = DataTransferObjects.OrdreOprettelse1.InvoicingMethod.invoice_by_mail,
                        PrimaryEmail = Email,
                        Apartment = String.Empty,
                        Attention = String.Empty,
                        CareOf = String.Empty,
                        Cityname = String.Empty,
                        Country = String.Empty,
                        Floor = String.Empty,
                        Housenumber = String.Empty,
                        SecondaryEmail = String.Empty,
                        Streetname = String.Empty,
                        Subcity = String.Empty,
                        Zipcode = String.Empty
                    };
                    customer.RewardData = new RewardDataDTO
                    {
                        CustomerNumber = 0,
                        ReferenceNumber = PartnerName,
                        ContractNumber = 0,
                        RewardAmount = 1,
                        Origin = Origin.partner_portal,
                        Reward = RewardType.New_Customer,
                        CalculationType = RewardCalculationType.Value,
                        Start_Date = DateTime.Now,
                        Stop_Date = new DateTime(2025, 12, 31),
                        PartnerName = "Campaign"
                    };
                }
                else
                {
                    customer.produkter = new List<ProduktDTO>()
                    {
                        new ProduktDTO
                        {
                            bynavn = CityName,
                            doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                            etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                            gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                            husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                            gratis_stroem_om_natten = false,
                            aftagenummer = String.Empty,
                            AdresseValideret = true,
                            amr = false,
                            binding = 6,
                            kontrakttype = (int)ContractTypes.el,
                            loft = false,
                            postnummer = ZipCode,
                            stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                            tillaeg = 10,
                            sparegaranti = true,
                            Partner =  new Guid("90036C08-F41A-4D98-95D6-F36D969E7AE6"),
                            Sælger = SellerID, // her skal ændres
                            opstartsdato = ConvertedDate,
                            Modul = OprettelsesModul.Unset,
                            valideringsStatus = ValideringsStatus.Registreret,
                            internalID = Guid.NewGuid(),
                            produkt_id = new Guid("8F8CCDDF-C382-44BF-A1EA-8073C7AE5BA7")
                        },
                        new ProduktDTO
                        {
                            bynavn = CityName,
                            doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                            etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                            gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                            husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                            gratis_stroem_om_natten = false,
                            aftagenummer = String.Empty,
                            AdresseValideret = true,
                            amr = false,
                            binding = 6,
                            kontrakttype = (int)ContractTypes.savingsAgreement,
                            loft = false,
                            postnummer = ZipCode,
                            stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                            tillaeg = 0,
                            sparegaranti = true,
                            Partner = new Guid("90036C08-F41A-4D98-95D6-F36D969E7AE6"),
                            Sælger = SellerID, // her skal SellerID bruges
                            opstartsdato = ConvertedDate,
                            Modul = OprettelsesModul.Unset,
                            valideringsStatus = ValideringsStatus.Registreret,
                            internalID = Guid.NewGuid(),
                            produkt_id = new Guid("C6E95B26-F67C-418B-A87A-448DAE20C5DC"),
                            ContractProperties = contractProp

                        },
                    };
                    customer.FaktureringsForm = new DataTransferObjects.OrdreOprettelse1.InvoicingMethodDTO
                    {
                        invoicingMethod = DataTransferObjects.OrdreOprettelse1.InvoicingMethod.invoice_by_mail,
                        PrimaryEmail = Email,
                        Apartment = String.Empty,
                        Attention = String.Empty,
                        CareOf = String.Empty,
                        Cityname = String.Empty,
                        Country = String.Empty,
                        Floor = String.Empty,
                        Housenumber = String.Empty,
                        SecondaryEmail = String.Empty,
                        Streetname = String.Empty,
                        Subcity = String.Empty,
                        Zipcode = String.Empty
                    };
                    customer.RewardData = new RewardDataDTO
                    {
                        CustomerNumber = 0,
                        ReferenceNumber = PartnerName,
                        ContractNumber = 0,
                        RewardAmount = 1,
                        Origin = Origin.partner_portal,
                        Reward = RewardType.New_Customer,
                        CalculationType = RewardCalculationType.Value,
                        Start_Date = ConvertedDate,
                        Stop_Date = new DateTime(2025, 12, 31),
                        PartnerName = "Campaign"
                    };
                }
                
                //Creation of the customer.
                HttpResponseMessage result = RequestHelper.APIPost("CustomerController", "CreatePreapprovedCustomer", null, new StringContent(JsonConvert.SerializeObject(customer).ToString(), Encoding.UTF8, "application/json"));
                if (result.IsSuccessStatusCode)
                {
                    DataTransferObjects.OrdreOprettelse.CustomerDTO customerCreated = JsonConvert.DeserializeObject<DataTransferObjects.OrdreOprettelse.CustomerDTO>(result.Content.ReadAsStringAsync().Result);
                    CustomerID = customerCreated.KundeID.ToString();
                    ObjectFactoryCore.Partner.CampaignPartner.LogCouponCustomer(customerCreated, coupon);
                    CustomerLbnr = customerCreated.Kundenummer;
                    CreateCommunicationLog(customerCreated.Kundenummer);
                    SendWelcomeMail(customerCreated, Enums.DocumentType.Velkomstmail_Partner_portal);
                }
                else
                {
                    errorMsg = result.Content.ReadAsStringAsync().Result;
                    var res = RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                     {
                        {"Message", $"Couldn't create customer: {errorMsg}"},
                        {"Location", "Modstroem.dk -> CampaignFormFlow" },
                        {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Besked).ToString()},
                     });
                    GeneralMethodes.SendEmail(errorMsg, emailToRecieveError, "Partner Campaign", "CampaignCoupon - Mit & NemID", "Partner Portal Status");
                    //LoggingLibrary.MessageLog.CreateLogEntry($"Couldn't create customer: {errorMsg}", "Modstroem.dk -> CampaignFormFlow", DataTransferObjects.Enums.MessageLogType.Besked);
                }
            }
            catch (Exception ex)
            {
                var res = RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                     {
                        {"Message", $"Customer creation failed. {ex.Message}"},
                        {"Location", "Modstroem.dk -> CampaignFormFlow" },
                        {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Exception).ToString()},
                     });
                // LoggingLibrary.MessageLog.CreateLogEntry($"Customer creation failed. {ex.Message}", "Modstroem.dk -> CampaignFormFlow", DataTransferObjects.Enums.MessageLogType.Exception);
            }
        }

        protected Boolean SendWelcomeMail(CustomerDTO Customer, Enums.DocumentType DocType)
        {
            DateTime opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate;
            try
            {
                List<QueueItem> queueItems = new List<QueueItem>();
                if (string.IsNullOrEmpty(PickedDate))
                {
                    AdditionelInfo addInfo = new AdditionelInfo
                    {
                        ticketInfo = new TicketInfo
                        {
                            Customernumber = Customer.Kundenummer,
                            Status = (int)TicketStatus.CLOSED,
                            Category = (int)TicketCategory.Andet,
                            Department = (int)TicketDepartment.Kunde,
                            Priority = (int)TicketPriority.Medium,
                            Responsible = 0
                        },
                        mergeData = new Dictionary<string, string>
                    {
                        {"[GAVEKORT-KODE]", $"{coupon}" },
                        {"[Forhandler]", $"{PartnerName}"},
                        {"[Leverancestart]", $"{opstartsdato.ToShortDateString()}"},
                    },

                    };

                    queueItems.Add(new QueueItem
                    {
                        SenderID = 2,
                        AddedBy = "CampaignFormFlow",
                        CreateTicket = true,
                        Instant = true,
                        customerNumber = Customer.Kundenummer,
                        ContractLbnr = Customer.produkter.First().Kontraktnummer,
                        Trackable = false,
                        documenttype = DocType,
                        documentFormat = Enums.DocumentFormat.HTML,
                        procesType = ProcessingType.Email,
                        additionelInfo = addInfo,
                    });
                }
                else
                {
                    AdditionelInfo addInfo = new AdditionelInfo
                    {
                        ticketInfo = new TicketInfo
                        {
                            Customernumber = Customer.Kundenummer,
                            Status = (int)TicketStatus.CLOSED,
                            Category = (int)TicketCategory.Andet,
                            Department = (int)TicketDepartment.Kunde,
                            Priority = (int)TicketPriority.Medium,
                            Responsible = 0
                        },
                        mergeData = new Dictionary<string, string>
                    {
                        {"[GAVEKORT-KODE]", $"{coupon}" },
                        {"[Forhandler]", $"{PartnerName}"},
                        {"[Leverancestart]", $"{ConvertedDate.ToShortDateString()}"},
                    },

                    };

                    queueItems.Add(new QueueItem
                    {
                        SenderID = 2,
                        AddedBy = "CampaignFormFlow",
                        CreateTicket = true,
                        Instant = true,
                        customerNumber = Customer.Kundenummer,
                        ContractLbnr = Customer.produkter.First().Kontraktnummer,
                        Trackable = false,
                        documenttype = DocType,
                        documentFormat = Enums.DocumentFormat.HTML,
                        procesType = ProcessingType.Email,
                        additionelInfo = addInfo,
                    });
                }
                
                var content = new StringContent(JsonConvert.SerializeObject(queueItems).ToString(), Encoding.UTF8, "application/json");
                var response = RequestHelper.APIPost("NotificationController", "SetDocumentsInQueue", null, content);

                if (response.IsSuccessStatusCode)
                {
                    RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                     {
                        {"Message", $"Succes to send Welcome Email for customer - return = true"},
                        {"Location", "Modstroem.dk -> CampaignFormFlow" },
                        {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Besked).ToString()},
                     });
                    return true;
                }
                else
                {
                    RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                     {
                        {"Message", $"Failed to send Welcome Email for customer - return = false"},
                        {"Location", "Modstroem.dk -> CampaignFormFlow" },
                        {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Fejl).ToString()},
                     });
                    return false;
                }
            }
            catch (Exception ex)
            {
                RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                     {
                        {"Message", $"Failed to send Welcome Email for customer - {ex.Message}"},
                        {"Location", "Modstroem.dk -> CampaignFormFlow" },
                        {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Exception).ToString()},
                     });
                return false;
            }
        }

        protected void CreateCommunicationLog(int customernumber)
        {
            try
            {

                IPAddress myIP = HttpContext.Connection.RemoteIpAddress;

                DataTransferObjects.Internal.AcceptIdentityDTO logInfo = new DataTransferObjects.Internal.AcceptIdentityDTO();
                logInfo.Content = String.Empty;
                logInfo.Customer_Lbnr = customernumber.ToString();
                logInfo.Origin_URL = "CampaignFormFlow";
                logInfo.AcceptpageType = 1;

                try
                {

                    IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
                    logInfo.Origin_IP = GetIPHost.HostName.ToString();
                }
                catch
                {
                    logInfo.Origin_IP = myIP.ToString();
                }

                StringContent content = new StringContent(JsonConvert.SerializeObject(logInfo).ToString(), Encoding.UTF8, "application/json");
                HttpResponseMessage result = RequestHelper.APIPost("IdentityController", "RegisterAcceptpageCommunication", null, content);

                if (result.IsSuccessStatusCode)
                {
                    RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                     {
                        {"Message", "Communication was successfully logged"},
                        {"Location", "Modstroem.dk -> CampaignFormFlow" },
                        {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Besked).ToString()},
                     });

                    //LoggingLibrary.MessageLog.CreateLogEntry($"Communication was successfully logged", "Modstroem.dk -> CampaignFormFlow", DataTransferObjects.Enums.MessageLogType.Besked);
                }

                else
                {
                    RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                     {
                        {"Message", $"Communication logging failed for customernumer {customernumber} - {result.Content.ReadAsStringAsync().Result}"},
                        {"Location", "Modstroem.dk -> CampaignFormFlow" },
                        {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Fejl).ToString()},
                     });
                    //LoggingLibrary.MessageLog.CreateLogEntry($"Communication logging failed for customernumer {customernumber} - {result.Content.ReadAsStringAsync().Result}", "Modstroem.dk -> CampaignFormFlow", DataTransferObjects.Enums.MessageLogType.Fejl);
                }

            }
            catch (Exception ex)
            {
                RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                     {
                        {"Message", $"Communication logging failed for customernumer {customernumber} - {ex.Message}"},
                        {"Location", "Modstroem.dk -> CampaignFormFlow" },
                        {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Exception).ToString()},
                     });
                //LoggingLibrary.MessageLog.CreateLogEntry($"Communication logging failed for customernumer {customernumber} - {ex.Message}", "Modstroem.dk -> CampaignFormFlow", DataTransferObjects.Enums.MessageLogType.Exception);
            }
        }
    }
}

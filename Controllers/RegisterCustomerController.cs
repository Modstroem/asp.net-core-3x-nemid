﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using aspnetcore_oidc.Models;
using Microsoft.AspNetCore.Authorization;
using ObjectFactoryCore.Partner;
using EFS_DAL.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http;
using DataTransferObjects.Communication;
using static DataTransferObjects.Enums;
using DataTransferObjects;
using RequestHelperCore;
using DataTransferObjects.OrdreOprettelse;
using System.Net;
using LibraryModstroemLogs;
using aspnetcore_oidc.Methodes;
using DataTransferObjects.QuickOrder;
using LibraryModstroemLogs.Models;
using ObjectFactoryCore.Customer;

namespace aspnetcore_oidc.Controllers
{
    public class RegisterCustomerController : Controller
    {
        public static string emailToRecieveError = "bka@modstroem.dk";
        private int Age = 0;

        private String CityName;
        private String Door;
        private String Floor;
        private String StreetName;
        private String HouseNumber;
        private String ZipCode;
        private String SubCity;

        private String errorMsg;

        private string Adresse = string.Empty;
        private string Cpr = String.Empty;
        private string RegNumber = String.Empty;
        private string AccountNumber = String.Empty;
        private string AltContact = String.Empty;
        private string Phone = String.Empty;
        private DateTime MoveDate = DateTime.MinValue;
        DateTime firstLevDate = DateTime.Now;
        private string PickedDate = string.Empty;
        private DateTime ConvertedDate;
        private string FullName = string.Empty;
        private string customerFirstName = string.Empty;
        private string customerLastName = string.Empty;
        private string Email = string.Empty;
        private string ProductQuery = string.Empty;
        private string Product = string.Empty;
        private string Form = string.Empty;
        private Boolean isEasyGreen = false;
        private Guid EgenSpot = new Guid("C1894661-FB89-465F-B3D7-B14068F25D53");
        private Guid Spot10 = new Guid("8F8CCDDF-C382-44BF-A1EA-8073C7AE5BA7");
        private Guid GroenSpare = new Guid("41ABC12A-011F-4DEA-96DF-F802D1119951");
        private Guid EasyGreenPartner = new Guid("C2395993-6786-4A39-9027-BE392ACE496C");

        private string CommunicationID = string.Empty;
        Boolean CustomerNotCreated = false;
        private int SellerID = 123415;
        private int EasyGreenSellerID = 123604;
        Models.AppSettings settings = new Models.AppSettings();
        private string CashBackValue = string.Empty;
        private string verfiedName = string.Empty;

        [Authorize] //is a way of telling the framework to only allow requests from authenticated users and if not already authenticated this kicks off the process
        public IActionResult Index([FromQuery(Name = "product")] string ProductType, [FromQuery(Name = "adresse")] string adresse, [FromQuery(Name = "streetname")] string streetname, [FromQuery(Name = "housenumber")] string housenumber, [FromQuery(Name = "floor")] string floor, [FromQuery(Name = "door")] string door, [FromQuery(Name = "subcity")] string subcity, [FromQuery(Name = "zipcode")] string zipcode, [FromQuery(Name = "cityname")] string cityname, [FromQuery(Name = "name")] string name, [FromQuery(Name = "cPR")] string cPR, [FromQuery(Name = "email")] string email, [FromQuery(Name = "aEgtefaelleSamlever")] string altContact, [FromQuery(Name = "regNr")] string regNr, [FromQuery(Name = "kontonummer")] string kontonummer, [FromQuery(Name = "telefonnummer")] string phone, [FromQuery(Name = "vaelgIndflytningsdato")] string indflytningsDate, [FromQuery(Name = "iseasygreen")] bool easygreen, [FromQuery(Name = "form")] string form, [FromQuery(Name = "communicationID")] string communicationID, [FromQuery(Name = "cashback")] string cashback)
        {

            if (User.Identity.IsAuthenticated && HttpContext.Session != null)
            {
                var age = User.Claims.Any(x => x.Type.ToLower().StartsWith("age")) ? User.Claims.Where(x => x.Type == "age").Select(x => x.Value).FirstOrDefault() : String.Empty;
                Int32.TryParse(age, out Age);
                var isYouth = User.Claims.Any(x => x.Type.StartsWith("gov:saml:attribute:IsYouthCert")) ? User.Claims.Select(x => x.Type.StartsWith("gov:saml:attribute:IsYouthCert")).FirstOrDefault().ToString().ToLower() : String.Empty;
                //var nAge = User.Claims.Where(x => x.Type == "gov:saml:attribute:IsYouthCert").Select(x => x.Value).FirstOrDefault();
                var cprk = User.Claims.Where(x => x.Type == "gov:saml:attribute:CprNumberIdentifier").Select(x => x.Value).FirstOrDefault();
                // Goverment verfied name
                verfiedName = User.Claims.Any(x => x.Type.StartsWith("name")) ? User.Claims.Select(x => x.Type.StartsWith("name")).FirstOrDefault().ToString() : String.Empty;

                if (string.IsNullOrEmpty(cPR))
                {
                    Logout().GetAwaiter().GetResult();
                    return Redirect($"{settings.Config.Keys.Error}");
                }

                if (string.IsNullOrEmpty(cprk))
                {
                    Logout().GetAwaiter().GetResult();
                    return Redirect($"{settings.Config.Keys.Error}");
                }

                string noDashCprFromQuery = GeneralMethodes.RemoveCprDash(cPR);
                string noDashCprMitID = GeneralMethodes.RemoveCprDash(cprk);
                bool isOldEnough = GeneralMethodes.ValidAge(Age, isYouth);

                if (isOldEnough)
                {
                    if (noDashCprFromQuery == noDashCprMitID)
                    {
                        Cpr = cprk;
                    }
                    else // mismatch cpr
                    {
                        //Logout the authenticated user.
                        Logout().GetAwaiter().GetResult();
                        return Redirect($"{settings.Config.Keys.RegisterCustomerMissMatchCPR}");
                    }
                }
                else // under the age of 18
                {
                    Logout().GetAwaiter().GetResult();//Logout the authenticated user.
                    return Redirect($"{settings.Config.Keys.RegisterCustomerUnderAge}");
                }

                PickedDate = indflytningsDate;
                ProductQuery = !string.IsNullOrEmpty(ProductType) ? ProductType.ToString() : string.Empty;
                Adresse = adresse;
                FullName = name;
                Email = email;
                AltContact = altContact;
                RegNumber = regNr;
                AccountNumber = kontonummer;
                Phone = phone;
                StreetName = streetname;
                HouseNumber = housenumber;
                Floor = floor;
                Door = door;
                SubCity = subcity;
                ZipCode = zipcode;
                CityName = cityname;
                CommunicationID = communicationID;
                isEasyGreen = easygreen;
                Form = !string.IsNullOrEmpty(form) ? form : string.Empty;

                if (!string.IsNullOrEmpty(cashback))
                {
                    CashBackValue = cashback;
                }
                if (!string.IsNullOrEmpty(ProductQuery))
                {
                    if (ProductQuery.Contains("?") || ProductQuery.Contains("&"))
                    {
                        if (ProductQuery.Contains("?"))
                        {
                            Product = ProductQuery.Replace("?", string.Empty);
                        }
                        else
                        {
                            Product = ProductQuery.Replace("&", string.Empty);
                        }
                    }
                    else
                    {
                        Product = ProductQuery.ToLower();
                    }
                }
                else
                {
                    Product = ProductQuery.ToLower();
                }

                if (!string.IsNullOrEmpty(PickedDate))
                {
                    ConvertedDate = Convert.ToDateTime(PickedDate);
                }

                if (!string.IsNullOrEmpty(FullName))
                {
                    var names = GeneralMethodes.SplitFullName(FullName);
                    customerFirstName = names.Item1;
                    customerLastName = names.Item2;
                }
            }
            return Protected();
        }
        // The [Authorize] is a way of telling the framework to only allow requests from authenticated users.
        [Authorize] // If not already authenticated this kicks off the process
        public IActionResult Protected()

        {   // authenticated user allowed.
            if (User.Identity.IsAuthenticated && HttpContext.Session != null)
            {
                CreateCustomer();//Creates the customer

                //if there is a product and no error
                if (isEasyGreen)
                {
                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        Logout().GetAwaiter().GetResult();//Logout the authenticated user.
                        return Redirect(string.Format($"{settings.Config.Keys.EasyGreenFeedback}"));//redirect
                    }
                    else
                    {
                        Logout().GetAwaiter().GetResult();//Logout the authenticated user.
                        return Redirect($"{settings.Config.Keys.RegisterCustomerError}");
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Product) && string.IsNullOrEmpty(errorMsg))
                    {
                        Logout().GetAwaiter().GetResult();//Logout the authenticated user.
                        return Redirect(string.Format($"{settings.Config.Keys.RegisterCustomerFeedback}?product={Product}&form={Form}"));//redirect with the product

                    }
                    else // no product found page redirection
                    {
                        Logout().GetAwaiter().GetResult();//Logout the authenticated user.
                        return Redirect($"{settings.Config.Keys.RegisterCustomerError}");
                    }
                }
            }

            return View();
        }

        public async Task Logout()
        {
            try
            {
                // Call the server to terminate the session
                await HttpContext.SignOutAsync(OpenIdConnectDefaults.AuthenticationScheme);
                // Remove authentication cookies
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            }
            catch (Exception ex)
            {
                Logging.CreateMessageLog($"Failed to Logout customer {Email} - {ex.Message}", "MitID -> RegisterCustomerFlow", (int)DataTransferObjects.Enums.MessageLogType.Exception);
                //var res = RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                //    {
                //    {"Message", $"Logout failed. {ex.Message}"},
                //    {"Location", "Modstroem.dk -> RegisterCustomerFlow" },
                //    {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Exception).ToString()},
                //    });
            }
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return Redirect($"{settings.Config.Keys.RegisterCustomerError}");
            //return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public void CreateCustomer()
        {
            try
            {
                if (DateTime.Now.Day < 18)
                {
                    firstLevDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(2);
                }
                else
                {
                    firstLevDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(3);
                }

                List<CustomerContractPropertyDTO> contractProp = new List<CustomerContractPropertyDTO>{
                    new CustomerContractPropertyDTO
                    {
                        IdContract = new Guid(),
                        IdAlias = (int)Enums.ContractPropertyAlias.ContractLength,
                        Value = "6"
                    }
                };

                int PaymentType = 0;

                if (!string.IsNullOrEmpty(CashBackValue))
                {
                    PaymentType = (int)Enums.PaymentForm.Giro_indbetaling;
                } else
                {
                    PaymentType = (int)Enums.PaymentForm.PBS;
                }

                CustomerDTO customer = new CustomerDTO
                {
                    CPRNummer = String.IsNullOrEmpty(Cpr) ? String.Empty : Cpr,
                    AdresseValideret = true,
                    AlternativKontaktperson = String.IsNullOrEmpty(AltContact) ? String.Empty : AltContact,
                    Bynavn = String.IsNullOrEmpty(CityName) ? String.Empty : CityName,
                    DoerBetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                    Efternavn = String.IsNullOrEmpty(customerLastName) ? String.Empty : customerLastName,
                    Fornavn = String.IsNullOrEmpty(customerFirstName) ? String.Empty : customerFirstName,
                    Etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                    Email = String.IsNullOrEmpty(Email) ? String.Empty : Email,
                    Gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                    Husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                    Postnummer = String.IsNullOrEmpty(ZipCode) ? String.Empty : ZipCode,
                    Telefon = String.IsNullOrEmpty(Phone) ? String.Empty : Phone,
                    Mobil = String.IsNullOrEmpty(Phone) ? String.Empty : Phone,
                    VPRNummer = String.Empty,
                    Firmanavn = String.Empty,
                    FirmakontaktPerson = String.Empty,
                    Stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                    Kundetype = (int)Enums.Segment.Private,
                    servicepakke = null,
                    BetalingsID = PaymentType,
                    Registreringsnummer = String.IsNullOrEmpty(RegNumber) ? String.Empty : RegNumber,
                    Kontonummer = String.IsNullOrEmpty(AccountNumber) ? String.Empty : AccountNumber,
                    relation = String.IsNullOrEmpty(AltContact) ? "Ingen" : "Samlever",
                    ReferralID = String.Empty,
                    Transaktionsnummer = String.Empty
                };

                if (isEasyGreen)
                {
                    if (string.IsNullOrEmpty(PickedDate))
                    {
                        customer.produkter = new List<ProduktDTO>()
                        {
                            new ProduktDTO
                            {
                                bynavn = CityName,
                                doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                gratis_stroem_om_natten = false,
                                aftagenummer = String.Empty,
                                AdresseValideret = true,
                                amr = false,
                                binding = 6,
                                kontrakttype = (int)ContractTypes.el,
                                loft = false,
                                postnummer = ZipCode,
                                stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                tillaeg = 10,
                                sparegaranti = true,
                                Partner =  EasyGreenPartner,
                                Sælger = EasyGreenSellerID,
                                opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                                Modul = OprettelsesModul.Unset,
                                valideringsStatus = ValideringsStatus.Registreret,
                                internalID = Guid.NewGuid(),
                                produkt_id = Spot10 // Spot 10 
                            },
                            new ProduktDTO
                            {
                                bynavn = CityName,
                                doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                gratis_stroem_om_natten = false,
                                aftagenummer = String.Empty,
                                AdresseValideret = true,
                                amr = false,
                                binding = 6,
                                kontrakttype = (int)ContractTypes.egenspot,
                                loft = false,
                                postnummer = ZipCode,
                                stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                tillaeg = 0,
                                sparegaranti = true,
                                Partner = EasyGreenPartner,
                                Sælger = EasyGreenSellerID,
                                opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                                Modul = OprettelsesModul.Unset,
                                valideringsStatus = ValideringsStatus.Registreret,
                                internalID = Guid.NewGuid(),
                                produkt_id = EgenSpot, // Egenspot 
                            },
                             new ProduktDTO
                            {
                                bynavn = CityName,
                                doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                gratis_stroem_om_natten = false,
                                aftagenummer = String.Empty,
                                AdresseValideret = true,
                                amr = false,
                                binding = 6,
                                kontrakttype = (int)ContractTypes.savingsAgreement,
                                loft = false,
                                postnummer = ZipCode,
                                stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                tillaeg = 0,
                                sparegaranti = true,
                                Partner = EasyGreenPartner,
                                Sælger = EasyGreenSellerID,
                                opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                                Modul = OprettelsesModul.Unset,
                                valideringsStatus = ValideringsStatus.Registreret,
                                internalID = Guid.NewGuid(),
                                produkt_id = GroenSpare, // Grøn Spareaftale (Klima) 
                                ContractProperties = contractProp
                            },
                        };
                        customer.FaktureringsForm = new DataTransferObjects.OrdreOprettelse1.InvoicingMethodDTO
                        {
                            invoicingMethod = DataTransferObjects.OrdreOprettelse1.InvoicingMethod.invoice_by_mail,
                            PrimaryEmail = Email,
                            Apartment = String.Empty,
                            Attention = String.Empty,
                            CareOf = String.Empty,
                            Cityname = String.Empty,
                            Country = String.Empty,
                            Floor = String.Empty,
                            Housenumber = String.Empty,
                            SecondaryEmail = String.Empty,
                            Streetname = String.Empty,
                            Subcity = String.Empty,
                            Zipcode = String.Empty
                        };
                        customer.RewardData = new RewardDataDTO
                        {
                            CustomerNumber = 0,
                           // ReferenceNumber = Product,
                            ContractNumber = 0,
                            RewardAmount = 1,
                            Origin = Origin.partner_portal,
                            Reward = RewardType.New_Customer,
                            CalculationType = RewardCalculationType.Value,
                            Start_Date = DateTime.Now,
                            Stop_Date = new DateTime(2025, 12, 31),
                            PartnerName = "EasyGreen"
                        };
                    }
                    else
                    {
                        customer.produkter = new List<ProduktDTO>()
                        {
                            new ProduktDTO
                            {
                                bynavn = CityName,
                                doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                gratis_stroem_om_natten = false,
                                aftagenummer = String.Empty,
                                AdresseValideret = true,
                                amr = false,
                                binding = 6,
                                kontrakttype = (int)ContractTypes.el,
                                loft = false,
                                postnummer = ZipCode,
                                stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                tillaeg = 10,
                                sparegaranti = true,
                                Partner =  EasyGreenPartner,
                                Sælger = EasyGreenSellerID,
                                opstartsdato = ConvertedDate,
                                Modul = OprettelsesModul.Unset,
                                valideringsStatus = ValideringsStatus.Registreret,
                                internalID = Guid.NewGuid(),
                                produkt_id = Spot10
                            },
                            new ProduktDTO
                            {
                                bynavn = CityName,
                                doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                gratis_stroem_om_natten = false,
                                aftagenummer = String.Empty,
                                AdresseValideret = true,
                                amr = false,
                                binding = 6,
                                kontrakttype = (int)ContractTypes.savingsAgreement,
                                loft = false,
                                postnummer = ZipCode,
                                stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                tillaeg = 0,
                                sparegaranti = true,
                                Partner = EasyGreenPartner,
                                Sælger = EasyGreenSellerID,
                                opstartsdato = ConvertedDate,
                                Modul = OprettelsesModul.Unset,
                                valideringsStatus = ValideringsStatus.Registreret,
                                internalID = Guid.NewGuid(),
                                produkt_id = EgenSpot,
                                ContractProperties = contractProp
                            },
                            new ProduktDTO
                            {
                                bynavn = CityName,
                                doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                gratis_stroem_om_natten = false,
                                aftagenummer = String.Empty,
                                AdresseValideret = true,
                                amr = false,
                                binding = 6,
                                kontrakttype = (int)ContractTypes.dgk,
                                loft = false,
                                postnummer = ZipCode,
                                stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                tillaeg = 0,
                                sparegaranti = true,
                                Partner = EasyGreenPartner,
                                Sælger = EasyGreenSellerID,
                                opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                                Modul = OprettelsesModul.Unset,
                                valideringsStatus = ValideringsStatus.Registreret,
                                internalID = Guid.NewGuid(),
                                produkt_id = GroenSpare, // Grøn Spareaftale (Klima) 
                            },
                        };
                        customer.FaktureringsForm = new DataTransferObjects.OrdreOprettelse1.InvoicingMethodDTO
                        {
                            invoicingMethod = DataTransferObjects.OrdreOprettelse1.InvoicingMethod.invoice_by_mail,
                            PrimaryEmail = Email,
                            Apartment = String.Empty,
                            Attention = String.Empty,
                            CareOf = String.Empty,
                            Cityname = String.Empty,
                            Country = String.Empty,
                            Floor = String.Empty,
                            Housenumber = String.Empty,
                            SecondaryEmail = String.Empty,
                            Streetname = String.Empty,
                            Subcity = String.Empty,
                            Zipcode = String.Empty
                        };
                        customer.RewardData = new RewardDataDTO
                        {
                            CustomerNumber = 0,
                           // ReferenceNumber = Product,
                            ContractNumber = 0,
                            RewardAmount = 1,
                            Origin = Origin.partner_portal,
                            Reward = RewardType.New_Customer,
                            CalculationType = RewardCalculationType.Value,
                            Start_Date = ConvertedDate,
                            Stop_Date = new DateTime(2025, 12, 31),
                        };
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(PickedDate))
                    {
                        if (Product == "plus")
                        {
                            customer.produkter = new List<ProduktDTO>()
                        {
                            new ProduktDTO
                            {
                                bynavn = CityName,
                                doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                gratis_stroem_om_natten = false,
                                aftagenummer = String.Empty,
                                AdresseValideret = true,
                                amr = false,
                                binding = 6,
                                kontrakttype = (int)ContractTypes.el,
                                loft = false,
                                postnummer = ZipCode,
                                stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                tillaeg = 10,
                                sparegaranti = true,
                                Partner =  new Guid("9e9b1c34-3563-4e68-b6f1-4ce1899bb0a4"),
                                Sælger = SellerID,
                                opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                                Modul = OprettelsesModul.Unset,
                                valideringsStatus = ValideringsStatus.Registreret,
                                internalID = Guid.NewGuid(),
                                produkt_id = new Guid("03A19E8D-1132-4BCA-9371-571ADB02BC9D")
                            },
                            new ProduktDTO
                            {
                                bynavn = CityName,
                                doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                gratis_stroem_om_natten = false,
                                aftagenummer = String.Empty,
                                AdresseValideret = true,
                                amr = false,
                                binding = 6,
                                kontrakttype = (int)ContractTypes.savingsAgreement,
                                loft = false,
                                postnummer = ZipCode,
                                stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                tillaeg = 0,
                                sparegaranti = true,
                                Partner = new Guid("9e9b1c34-3563-4e68-b6f1-4ce1899bb0a4"),
                                Sælger = SellerID,
                                opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                                Modul = OprettelsesModul.Unset,
                                valideringsStatus = ValideringsStatus.Registreret,
                                internalID = Guid.NewGuid(),
                                produkt_id = new Guid("4F0C2E09-DCAE-4315-9ACF-74B2D0F0BC38"),
                                ContractProperties = contractProp

                            },
                        };
                        }
                        if (Product == "klima")
                        {
                            customer.produkter = new List<ProduktDTO>()
                        {
                            new ProduktDTO
                            {
                                bynavn = CityName,
                                doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                gratis_stroem_om_natten = false,
                                aftagenummer = String.Empty,
                                AdresseValideret = true,
                                amr = false,
                                binding = 6,
                                kontrakttype = (int)ContractTypes.el,
                                loft = false,
                                postnummer = ZipCode,
                                stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                tillaeg = 10,
                                sparegaranti = true,
                                Partner =  new Guid("9e9b1c34-3563-4e68-b6f1-4ce1899bb0a4"),
                                Sælger = SellerID,
                                opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                                Modul = OprettelsesModul.Unset,
                                valideringsStatus = ValideringsStatus.Registreret,
                                internalID = Guid.NewGuid(),
                                produkt_id = new Guid("03A19E8D-1132-4BCA-9371-571ADB02BC9D")
                            },
                            new ProduktDTO
                            {
                                bynavn = CityName,
                                doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                gratis_stroem_om_natten = false,
                                aftagenummer = String.Empty,
                                AdresseValideret = true,
                                amr = false,
                                binding = 6,
                                kontrakttype = (int)ContractTypes.savingsAgreement,
                                loft = false,
                                postnummer = ZipCode,
                                stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                tillaeg = 0,
                                sparegaranti = true,
                                Partner = new Guid("9e9b1c34-3563-4e68-b6f1-4ce1899bb0a4"),
                                Sælger = SellerID,
                                opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                                Modul = OprettelsesModul.Unset,
                                valideringsStatus = ValideringsStatus.Registreret,
                                internalID = Guid.NewGuid(),
                                produkt_id = new Guid("C1894661-FB89-465F-B3D7-B14068F25D53"),
                                ContractProperties = contractProp

                            },
                        };
                        }
                        if (Product == "premium")
                        {
                            customer.produkter = new List<ProduktDTO>()
                        {
                            new ProduktDTO
                            {
                                bynavn = CityName,
                                doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                gratis_stroem_om_natten = false,
                                aftagenummer = String.Empty,
                                AdresseValideret = true,
                                amr = false,
                                binding = 6,
                                kontrakttype = (int)ContractTypes.el,
                                loft = false,
                                postnummer = ZipCode,
                                stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                tillaeg = 10,
                                sparegaranti = true,
                                Partner =  new Guid("9e9b1c34-3563-4e68-b6f1-4ce1899bb0a4"),
                                Sælger = SellerID,
                                opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                                Modul = OprettelsesModul.Unset,
                                valideringsStatus = ValideringsStatus.Registreret,
                                internalID = Guid.NewGuid(),
                                produkt_id = new Guid("03A19E8D-1132-4BCA-9371-571ADB02BC9D")
                            },
                            new ProduktDTO
                            {
                                bynavn = CityName,
                                doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                gratis_stroem_om_natten = false,
                                aftagenummer = String.Empty,
                                AdresseValideret = true,
                                amr = false,
                                binding = 6,
                                kontrakttype = (int)ContractTypes.savingsAgreement,
                                loft = false,
                                postnummer = ZipCode,
                                stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                tillaeg = 0,
                                sparegaranti = true,
                                Partner = new Guid("9e9b1c34-3563-4e68-b6f1-4ce1899bb0a4"),
                                Sælger = SellerID,
                                opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate,
                                Modul = OprettelsesModul.Unset,
                                valideringsStatus = ValideringsStatus.Registreret,
                                internalID = Guid.NewGuid(),
                                produkt_id = new Guid("7FEB347E-E6E2-4859-8813-10A480850E88"),
                                ContractProperties = contractProp

                            },
                        };
                        }

                        customer.FaktureringsForm = new DataTransferObjects.OrdreOprettelse1.InvoicingMethodDTO
                        {
                            invoicingMethod = DataTransferObjects.OrdreOprettelse1.InvoicingMethod.invoice_by_mail,
                            PrimaryEmail = Email,
                            Apartment = String.Empty,
                            Attention = String.Empty,
                            CareOf = String.Empty,
                            Cityname = String.Empty,
                            Country = String.Empty,
                            Floor = String.Empty,
                            Housenumber = String.Empty,
                            SecondaryEmail = String.Empty,
                            Streetname = String.Empty,
                            Subcity = String.Empty,
                            Zipcode = String.Empty
                        };
                        customer.RewardData = new RewardDataDTO
                        {
                            CustomerNumber = 0,
                            ReferenceNumber = Product,
                            ContractNumber = 0,
                            RewardAmount = 1,
                            Origin = Origin.partner_portal,
                            Reward = RewardType.New_Customer,
                            CalculationType = RewardCalculationType.Value,
                            Start_Date = DateTime.Now,
                            Stop_Date = new DateTime(2025, 12, 31),
                        };
                    }
                    else
                    {
                        if (Product == "plus")
                        {
                            customer.produkter = new List<ProduktDTO>()
                            {
                                new ProduktDTO
                                {
                                    bynavn = CityName,
                                    doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                    etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                    gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                    husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                    gratis_stroem_om_natten = false,
                                    aftagenummer = String.Empty,
                                    AdresseValideret = true,
                                    amr = false,
                                    binding = 6,
                                    kontrakttype = (int)ContractTypes.el,
                                    loft = false,
                                    postnummer = ZipCode,
                                    stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                    tillaeg = 10,
                                    sparegaranti = true,
                                    Partner =  new Guid("9e9b1c34-3563-4e68-b6f1-4ce1899bb0a4"),
                                    Sælger = SellerID,
                                    opstartsdato = ConvertedDate,
                                    Modul = OprettelsesModul.Unset,
                                    valideringsStatus = ValideringsStatus.Registreret,
                                    internalID = Guid.NewGuid(),
                                    produkt_id = new Guid("03A19E8D-1132-4BCA-9371-571ADB02BC9D")
                                },
                                new ProduktDTO
                                {
                                    bynavn = CityName,
                                    doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                    etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                    gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                    husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                    gratis_stroem_om_natten = false,
                                    aftagenummer = String.Empty,
                                    AdresseValideret = true,
                                    amr = false,
                                    binding = 6,
                                    kontrakttype = (int)ContractTypes.savingsAgreement,
                                    loft = false,
                                    postnummer = ZipCode,
                                    stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                    tillaeg = 0,
                                    sparegaranti = true,
                                    Partner = new Guid("9e9b1c34-3563-4e68-b6f1-4ce1899bb0a4"),
                                    Sælger = SellerID,
                                    opstartsdato = ConvertedDate,
                                    Modul = OprettelsesModul.Unset,
                                    valideringsStatus = ValideringsStatus.Registreret,
                                    internalID = Guid.NewGuid(),
                                    produkt_id = new Guid("4F0C2E09-DCAE-4315-9ACF-74B2D0F0BC38"),
                                    ContractProperties = contractProp

                                },
                            };
                        }
                        if (Product == "klima")
                        {
                            customer.produkter = new List<ProduktDTO>()
                            {
                                new ProduktDTO
                                {
                                    bynavn = CityName,
                                    doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                    etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                    gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                    husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                    gratis_stroem_om_natten = false,
                                    aftagenummer = String.Empty,
                                    AdresseValideret = true,
                                    amr = false,
                                    binding = 6,
                                    kontrakttype = (int)ContractTypes.el,
                                    loft = false,
                                    postnummer = ZipCode,
                                    stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                    tillaeg = 10,
                                    sparegaranti = true,
                                    Partner =  new Guid("9e9b1c34-3563-4e68-b6f1-4ce1899bb0a4"),
                                    Sælger = SellerID,
                                    opstartsdato = ConvertedDate,
                                    Modul = OprettelsesModul.Unset,
                                    valideringsStatus = ValideringsStatus.Registreret,
                                    internalID = Guid.NewGuid(),
                                    produkt_id = new Guid("03A19E8D-1132-4BCA-9371-571ADB02BC9D")
                                },
                                new ProduktDTO
                                {
                                    bynavn = CityName,
                                    doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                    etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                    gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                    husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                    gratis_stroem_om_natten = false,
                                    aftagenummer = String.Empty,
                                    AdresseValideret = true,
                                    amr = false,
                                    binding = 6,
                                    kontrakttype = (int)ContractTypes.savingsAgreement,
                                    loft = false,
                                    postnummer = ZipCode,
                                    stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                    tillaeg = 0,
                                    sparegaranti = true,
                                    Partner = new Guid("9e9b1c34-3563-4e68-b6f1-4ce1899bb0a4"),
                                    Sælger = SellerID,
                                    opstartsdato = ConvertedDate,
                                    Modul = OprettelsesModul.Unset,
                                    valideringsStatus = ValideringsStatus.Registreret,
                                    internalID = Guid.NewGuid(),
                                    produkt_id = new Guid("C1894661-FB89-465F-B3D7-B14068F25D53"),
                                    ContractProperties = contractProp

                                },
                            };
                        }
                        if (Product == "premium")
                        {
                            customer.produkter = new List<ProduktDTO>()
                            {
                                new ProduktDTO
                                {
                                    bynavn = CityName,
                                    doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                    etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                    gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                    husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                    gratis_stroem_om_natten = false,
                                    aftagenummer = String.Empty,
                                    AdresseValideret = true,
                                    amr = false,
                                    binding = 6,
                                    kontrakttype = (int)ContractTypes.el,
                                    loft = false,
                                    postnummer = ZipCode,
                                    stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                    tillaeg = 10,
                                    sparegaranti = true,
                                    Partner =  new Guid("9e9b1c34-3563-4e68-b6f1-4ce1899bb0a4"),
                                    Sælger = SellerID,
                                    opstartsdato = ConvertedDate,
                                    Modul = OprettelsesModul.Unset,
                                    valideringsStatus = ValideringsStatus.Registreret,
                                    internalID = Guid.NewGuid(),
                                    produkt_id = new Guid("03A19E8D-1132-4BCA-9371-571ADB02BC9D")
                                },
                                new ProduktDTO
                                {
                                    bynavn = CityName,
                                    doerbetegnelse = String.IsNullOrEmpty(Door) ? String.Empty : Door,
                                    etage = String.IsNullOrEmpty(Floor) ? String.Empty : Floor,
                                    gadenavn = String.IsNullOrEmpty(StreetName) ? String.Empty : StreetName,
                                    husnummer = String.IsNullOrEmpty(HouseNumber) ? String.Empty : HouseNumber,
                                    gratis_stroem_om_natten = false,
                                    aftagenummer = String.Empty,
                                    AdresseValideret = true,
                                    amr = false,
                                    binding = 6,
                                    kontrakttype = (int)ContractTypes.savingsAgreement,
                                    loft = false,
                                    postnummer = ZipCode,
                                    stedbetegnelse = String.IsNullOrEmpty(SubCity) ? String.Empty : SubCity,
                                    tillaeg = 0,
                                    sparegaranti = true,
                                    Partner = new Guid("9e9b1c34-3563-4e68-b6f1-4ce1899bb0a4"),
                                    Sælger = SellerID,
                                    opstartsdato = ConvertedDate,
                                    Modul = OprettelsesModul.Unset,
                                    valideringsStatus = ValideringsStatus.Registreret,
                                    internalID = Guid.NewGuid(),
                                    produkt_id = new Guid("7FEB347E-E6E2-4859-8813-10A480850E88"),
                                    ContractProperties = contractProp

                                },
                            };
                        }

                        customer.FaktureringsForm = new DataTransferObjects.OrdreOprettelse1.InvoicingMethodDTO
                        {
                            invoicingMethod = DataTransferObjects.OrdreOprettelse1.InvoicingMethod.invoice_by_mail,
                            PrimaryEmail = Email,
                            Apartment = String.Empty,
                            Attention = String.Empty,
                            CareOf = String.Empty,
                            Cityname = String.Empty,
                            Country = String.Empty,
                            Floor = String.Empty,
                            Housenumber = String.Empty,
                            SecondaryEmail = String.Empty,
                            Streetname = String.Empty,
                            Subcity = String.Empty,
                            Zipcode = String.Empty
                        };
                        customer.RewardData = new RewardDataDTO
                        {
                            CustomerNumber = 0,
                            ReferenceNumber = Product,
                            ContractNumber = 0,
                            RewardAmount = 1,
                            Origin = Origin.partner_portal,
                            Reward = RewardType.New_Customer,
                            CalculationType = RewardCalculationType.Value,
                            Start_Date = ConvertedDate,
                            Stop_Date = new DateTime(2025, 12, 31),
                        };
                    }
                }

                if (!string.IsNullOrEmpty(CashBackValue))
                {
                    customer.CustomerProperties = new List<DataTransferObjects.Table.CustomerPropertyDTO>
                    {
                        new DataTransferObjects.Table.CustomerPropertyDTO
                        {
                            CreatedDate = DateTime.Now,
                            IdAlias = CustomerPropertyAlias.CashbackAgreement,
                            Value = CashBackValue,
                            CustomerNumber = 0,
                            UpdatedDate = DateTime.Now,
                        }
                    };
                }


                HttpResponseMessage result = RequestHelper.APIPost("CustomerController", "CreatePreapprovedCustomer", null, new StringContent(JsonConvert.SerializeObject(customer).ToString(), Encoding.UTF8, "application/json"));
                if (result.IsSuccessStatusCode)
                {
                    DataTransferObjects.OrdreOprettelse.CustomerDTO customerCreated = JsonConvert.DeserializeObject<DataTransferObjects.OrdreOprettelse.CustomerDTO>(result.Content.ReadAsStringAsync().Result);

                    CreateCommunicationLog(CommunicationID, customerCreated);

                    int cashIntVal = 0;

                    if (int.TryParse(CashBackValue, out cashIntVal))
                    {
                        Enums.CashbackAgreement cashbackAgreement = (Enums.CashbackAgreement)cashIntVal;

                        if (cashbackAgreement == CashbackAgreement.LoyaltyKey)
                        {
                            List<QueueItem> queueItems = new List<QueueItem>();

                            queueItems.Add(new QueueItem
                            {
                                SenderID = 2,
                                AddedBy = "RegisterCustomer",
                                Instant = true,
                                customerNumber = customerCreated.Kundenummer,
                                Trackable = true,
                                documenttype = Enums.DocumentType.LoyaltyKey,
                                documentFormat = Enums.DocumentFormat.HTML,
                                procesType = ProcessingType.Email,
                                additionelInfo = new AdditionelInfo
                                {
                                    ticketInfo = new TicketInfo
                                    {
                                        Customernumber = customerCreated.Kundenummer,
                                        Category = (int)TicketCategory.Kundeservice,
                                        Department = (int)TicketDepartment.CS,
                                        Priority = (int)TicketPriority.Medium,
                                        Responsible = (int)TicketResponsible.Kundeservice,
                                        Status = (int)TicketStatus.CLOSED,
                                        Title = "Tilmeld dit betalingskort",
                                        Description = String.Empty
                                    },
                                    mergeData = new Dictionary<string, string> 
                                    { 
                                        { 
                                            "CustomerId", customerCreated.KundeID.ToString() 
                                        } 
                                    }
                                },
                                CreateTicket = true
                            });

                            var content = new StringContent(JsonConvert.SerializeObject(queueItems).ToString(), Encoding.UTF8, "application/json");
                            var response = RequestHelper.APIPost("NotificationController", "SetDocumentsInQueue", null, content);


                            if (response.IsSuccessStatusCode)
                            {
                                Logging.CreateMessageLog($"Succes to send Loyalty Email {customerCreated.Kundenummer},  Response: {response.Content.ReadAsStringAsync().Result}", "MitID -> Modstroem.dk", (int)DataTransferObjects.Enums.MessageLogType.Besked);
                            }
                            else
                            {
                                Logging.CreateMessageLog($"Failed to send Loyalty Email {customerCreated.Kundenummer} Response: {response.Content.ReadAsStringAsync().Result}", "MitID -> Modstroem.dk", (int)DataTransferObjects.Enums.MessageLogType.Fejl);
                            }
                        }
                    }

                    if (isEasyGreen)
                    {
                        CreateTicket(customerCreated);
                        SendWelcomeMail(customerCreated, Enums.DocumentType.Velkomstmail_Klima_klima);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(PickedDate))
                        {
                            CreateTicket(customerCreated);
                        }
                        if (Product == "plus")
                        {
                            SendWelcomeMail(customerCreated, Enums.DocumentType.Velkomstmail_Klima_plus);
                        }
                        if (Product == "klima")
                        {
                            SendWelcomeMail(customerCreated, Enums.DocumentType.Velkomstmail_Klima_klima);
                        }
                        if (Product == "premium")
                        {
                            SendWelcomeMail(customerCreated, Enums.DocumentType.Velkomstmail_Klima_premium);
                        }
                    }
                }
                else
                {
                    errorMsg = result.Content.ReadAsStringAsync().Result;

                    Logging.CreateMessageLog($"Failed to create customer {Email} - {errorMsg}", "MitID -> RegisterCustomerFlow", (int)DataTransferObjects.Enums.MessageLogType.Fejl);

                    //var res = RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                    //{
                    //{"Message", $"Couldn't create customer: {errorMsg}"},
                    //{"Location", "Modstroem.dk -> RegisterCustomerFlow" },
                    //{"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Besked).ToString()},
                    //});
                    GeneralMethodes.SendEmail(errorMsg, emailToRecieveError, "Register Customer", "RegisterCustomer - Mit&NemID", "Register Customer status");
                }
            }
            catch (Exception ex)
            {
                Logging.CreateMessageLog($"Failed to create customer {Email} - {ex.Message}", "MitID -> RegisterCustomerFlow", (int)DataTransferObjects.Enums.MessageLogType.Exception);

                var res = RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                    {
                    {"Message", $"Customer creation failed. {ex.Message}"},
                    {"Location", "Modstroem.dk -> RegisterCustomerFlow" },
                    {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Exception).ToString()},
                    });
            }
        }

        protected Boolean SendWelcomeMail(CustomerDTO Customer, Enums.DocumentType DocType)
        {
            DateTime opstartsdato = MoveDate == DateTime.MinValue ? firstLevDate : MoveDate;
            try
            {
                List<QueueItem> queueItems = new List<QueueItem>();
                if (string.IsNullOrEmpty(PickedDate))
                {
                    AdditionelInfo addInfo = new AdditionelInfo
                    {
                        ticketInfo = new TicketInfo
                        {
                            Customernumber = Customer.Kundenummer,
                            Status = (int)TicketStatus.CLOSED,
                            Category = (int)TicketCategory.Andet,
                            Department = (int)TicketDepartment.Kunde,
                            Priority = (int)TicketPriority.Medium,
                            Responsible = 0
                        },
                        mergeData = new Dictionary<string, string>
                        {
                            {"[NAVN]", $"{FullName}" },
                            //{"[Forhandler]", $"{PartnerName}"},
                            {"[Leverancestart]", $"{opstartsdato.ToShortDateString()}"},
                        },
                    };

                    queueItems.Add(new QueueItem
                    {
                        SenderID = 2,
                        AddedBy = "RegisterCustomerFlow",
                        CreateTicket = true,
                        Instant = true,
                        customerNumber = Customer.Kundenummer,
                        ContractLbnr = Customer.produkter.First().Kontraktnummer,
                        Trackable = false,
                        documenttype = DocType,
                        documentFormat = Enums.DocumentFormat.HTML,
                        procesType = ProcessingType.Email,
                        additionelInfo = addInfo,
                    });
                }
                else
                {
                    AdditionelInfo addInfo = new AdditionelInfo
                    {
                        ticketInfo = new TicketInfo
                        {
                            Customernumber = Customer.Kundenummer,
                            Status = (int)TicketStatus.CLOSED,
                            Category = (int)TicketCategory.Andet,
                            Department = (int)TicketDepartment.Kunde,
                            Priority = (int)TicketPriority.Medium,
                            Responsible = 0
                        },
                        mergeData = new Dictionary<string, string>
                        {
                            {"[NAVN]", $"{FullName}" },
                            {"[Leverancestart]", $"{ConvertedDate.ToShortDateString()}"},
                        },
                    };

                    queueItems.Add(new QueueItem
                    {
                        SenderID = 2,
                        AddedBy = "RegisterCustomerFlow",
                        CreateTicket = true,
                        Instant = true,
                        customerNumber = Customer.Kundenummer,
                        ContractLbnr = Customer.produkter.First().Kontraktnummer,
                        Trackable = false,
                        documenttype = DocType,
                        documentFormat = Enums.DocumentFormat.HTML,
                        procesType = ProcessingType.Email,
                        additionelInfo = addInfo,
                    });
                }
                

                var content = new StringContent(JsonConvert.SerializeObject(queueItems).ToString(), Encoding.UTF8, "application/json");
                var response = RequestHelper.APIPost("NotificationController", "SetDocumentsInQueue", null, content);

                if (response.IsSuccessStatusCode)
                {
                    Logging.CreateMessageLog($"Succes to send Welcome Email {Customer.Kundenummer}", "MitID -> RegisterCustomerFlow", (int)DataTransferObjects.Enums.MessageLogType.Besked);

                    //RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                    //{
                    //{"Message", $"Succes to send Welcome Email for customer - return = true"},
                    //{"Location", "Modstroem.dk -> RegisterCustomerFlow" },
                    //{"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Besked).ToString()},
                    //});
                    return true;
                }
                else
                {
                    Logging.CreateMessageLog($"Failed to send Welcome Email {Customer.Kundenummer}", "MitID -> RegisterCustomerFlow", (int)DataTransferObjects.Enums.MessageLogType.Fejl);

                    //RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                    //{
                    //{"Message", $"Failed to send Welcome Email for customer - return = false"},
                    //{"Location", "Modstroem.dk -> RegisterCustomerFlow" },
                    //{"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Fejl).ToString()},
                    //});
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logging.CreateMessageLog($"Failed to send Welcome Email {Customer.Kundenummer} - {ex.Message}", "MitID -> RegisterCustomerFlow", (int)DataTransferObjects.Enums.MessageLogType.Exception);

                //RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                //    {
                //    {"Message", $"Failed to send Welcome Email for customer - {ex.Message}"},
                //    {"Location", "Modstroem.dk -> RegisterCustomerFlow" },
                //    {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Exception).ToString()},
                //    });
                return false;
            }
        }

        protected void CreateTicket(CustomerDTO Customer)
        {
            DateTime Date = new DateTime();
            if (string.IsNullOrEmpty(PickedDate))
            {
                Date = DateTime.Now;
            }
            else
            {
                Date = Convert.ToDateTime(PickedDate);
            }

            string titleTime = string.Empty;
            string titel = string.Empty;
            int Responsible = 0;
            string apiKey = settings.Config.Keys.APIKey;
            string encryptionKey = settings.Config.Keys.EncryptionKey;
            Dictionary<string, string> keys = new Dictionary<string, string>();
            keys.Add(apiKey, encryptionKey);
            if (isEasyGreen)
            {
                titel = "Egenproduktion - Egenproduktion";
                Responsible = 398110;
            }
            else
            {
                titel = "Tilflytning ";
                Responsible = 65187;
                titleTime = Date.ToString("dd/MM/yyyy");
            }
            
            try
            {

                TicketInfo ticket = new TicketInfo
                {
                    Customernumber = Customer.Kundenummer,
                    Status = (int)TicketStatus.OPEN,
                    Category = (int)TicketCategory.Aktiveringer,
                    Department = (int)TicketDepartment.Kunde,
                    Priority = (int)TicketPriority.Medium,
                    Responsible = Responsible,
                    Title = titel +" "+ titleTime,
                    Description = $"Navn: {FullName}{Environment.NewLine}Adresse: {StreetName} {HouseNumber} {SubCity} {ZipCode} {CityName}"
                };

                var content = new StringContent(JsonConvert.SerializeObject(ticket).ToString(), Encoding.UTF8, "application/json");
                var response = RequestHelper.APIPost($"CommunicationController", "CreateTicket", keys, content);

                if (response.IsSuccessStatusCode)
                {
                    Logging.CreateMessageLog($"Succes to create ticket", "MitID -> RegisterCustomerFlow", (int)DataTransferObjects.Enums.MessageLogType.Besked);

                    //RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                    // {
                    //    {"Message", $"Succes to create ticket"},
                    //    {"Location", "Modstroem.dk -> RegisterCustomerFlow" },
                    //    {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Besked).ToString()},
                    // });
                }
                else
                {
                    Logging.CreateMessageLog($"Failed to create ticket", "MitID -> RegisterCustomerFlow", (int)DataTransferObjects.Enums.MessageLogType.Fejl);

                    //RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                    // {
                    //    {"Message", $"Failed to create ticket"},
                    //    {"Location", "Modstroem.dk -> RegisterCustomerFlow" },
                    //    {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Fejl).ToString()},
                    // });
                }
            }
            catch (Exception ex)
            {
                Logging.CreateMessageLog($"Failed to create ticket - {ex.Message}", "MitID -> RegisterCustomerFlow", (int)DataTransferObjects.Enums.MessageLogType.Exception);

                //RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                //     {
                //        {"Message", $"Failed to create ticket - {ex.Message}"},
                //        {"Location", "Modstroem.dk -> RegisterCustomerFlow" },
                //        {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Exception).ToString()},
                //     });
            }
        }

        protected void CreateCommunicationLog(string communicationID, CustomerDTO customer)
        {
            try
            {
                LibraryModstroemLogs.Logging.UpdateCustomerLBNR(communicationID, customer.Kundenummer.ToString());

                string ip = string.Empty;
                IPAddress myIP = HttpContext.Connection.RemoteIpAddress;

                DataTransferObjects.Internal.AcceptIdentityDTO logInfo = new DataTransferObjects.Internal.AcceptIdentityDTO();
                logInfo.Customer_Lbnr = customer.Kundenummer.ToString();
                logInfo.Origin_URL = "RegisterCustomerFlow";
                logInfo.AcceptpageType = 1;

                try
                {

                    IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
                    logInfo.Origin_IP = GetIPHost.HostName.ToString();
                    ip = GetIPHost.HostName.ToString();
                }
                catch
                {
                    logInfo.Origin_IP = myIP.ToString();
                }

                StringContent content = new StringContent(JsonConvert.SerializeObject(logInfo).ToString(), Encoding.UTF8, "application/json");
                HttpResponseMessage result = RequestHelper.APIPost("IdentityController", "RegisterAcceptpageCommunication", null, content);

                if (result.IsSuccessStatusCode)
                {
                    GeneralMethodes.GetCommunicationID(ip, customer, verfiedName);
                    Logging.CreateMessageLog($"Communication was successfully logged for customer: {customer.Kundenummer}", "MitID -> RegisterCustomerFlow", (int)DataTransferObjects.Enums.MessageLogType.Besked);

                }
                else
                {
                    Logging.CreateMessageLog($"Communication logging failed for customer: {customer.Kundenummer} - {result.Content.ReadAsStringAsync().Result}", "MitID -> RegisterCustomerFlow", (int)DataTransferObjects.Enums.MessageLogType.Fejl);
                }

                string mitIDVerfiedData = $"sideSkift Navn:{verfiedName}, CPR: {Cpr}";
                //byte[] data = Convert.FromBase64String(mitIDVerfiedData);
                //string encodedHtml = Convert.ToBase64String(Encoding.UTF8.GetBytes(data));

                DateTime currentDate = DateTime.Now;
                string formattedDate = currentDate.ToString("yyyy-MM-dd");

                string apiUrl = $"{settings.Config.Keys.MinSideApi}/Logging/CreateCommunicationPost?" +
                             $"method=1&type=25&time={formattedDate}&" +
                             $"direction=1&ip={ip}&customerLbnr={customer.Kundenummer}&destination=MitID";

                using (HttpClient client = new HttpClient())
                {
                    StringContent contents = new StringContent(JsonConvert.SerializeObject(mitIDVerfiedData), UnicodeEncoding.UTF8, "application/json");
                    try
                    {
                        var results = client.PostAsync(apiUrl, content).Result;
                        var responseContent = result.Content.ReadAsStringAsync().Result;

                        if (!result.IsSuccessStatusCode)
                        {
                            Logging.CreateMessageLog($"Failed CommunicationID: {responseContent} at {DateTime.UtcNow}", "Modstroem.dk -> CustomerSignUpFlow -> GetCommunicationID()", (int)Enums.MessageLogType.Fejl);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.CreateMessageLog($"GetCommunicationID ERROR at {DateTime.UtcNow} Message: {ex.InnerException.InnerException.Message}", "Modstroem.dk -> CustomerSignUpFlow -> GetCommunicationID()", (int)Enums.MessageLogType.Exception);
                    }
                }

            }
            catch (Exception ex)
            {
                Logging.CreateMessageLog($"Communication logging failed for customernumer {customer.Kundenummer} - {ex.Message}", "MitID -> RegisterCustomerFlow", (int)DataTransferObjects.Enums.MessageLogType.Exception);
            }
        }
        //protected void CreateCommunicationLog(int customernumber)
        //{
        //    try
        //    {

        //        IPAddress myIP = HttpContext.Connection.RemoteIpAddress;

        //        DataTransferObjects.Internal.AcceptIdentityDTO logInfo = new DataTransferObjects.Internal.AcceptIdentityDTO();
        //        logInfo.Content = String.Empty;
        //        logInfo.Customer_Lbnr = customernumber.ToString();
        //        logInfo.Origin_URL = "RegisterCustomerFlow";
        //        logInfo.AcceptpageType = 1;

        //        try
        //        {

        //            IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
        //            logInfo.Origin_IP = GetIPHost.HostName.ToString();
        //        }
        //        catch
        //        {
        //            logInfo.Origin_IP = myIP.ToString();
        //        }

        //        StringContent content = new StringContent(JsonConvert.SerializeObject(logInfo).ToString(), Encoding.UTF8, "application/json");
        //        HttpResponseMessage result = RequestHelper.APIPost("IdentityController", "RegisterAcceptpageCommunication", null, content);

        //        if (result.IsSuccessStatusCode)
        //        {
        //            Logging.CreateMessageLog($"Communication was successfully logged for customer: {customernumber}", "", "RegisterCustomerFlow -> Modstroem.dk", (int)DataTransferObjects.Enums.MessageLogType.Besked);

        //            //RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
        //            //{
        //            //{"Message", "Communication was successfully logged"},
        //            //{"Location", "Modstroem.dk -> RegisterCustomerFlow" },
        //            //{"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Besked).ToString()},
        //            //});
        //        }
        //        else
        //        {
        //            Logging.CreateMessageLog($"Communication logging failed for customer: {customernumber} - {result.Content.ReadAsStringAsync().Result}", "", "RegisterCustomerFlow -> Modstroem.dk", (int)DataTransferObjects.Enums.MessageLogType.Fejl);

        //            //RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
        //            //{
        //            //{"Message", $"Communication logging failed for customernumer {customernumber} - {result.Content.ReadAsStringAsync().Result}"},
        //            //{"Location", "Modstroem.dk -> RegisterCustomerFlow" },
        //            //{"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Fejl).ToString()},
        //            //});
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Logging.CreateMessageLog($"Communication logging failed for customernumer {customernumber} - {ex.Message}", "", "RegisterCustomerFlow -> Modstroem.dk", (int)DataTransferObjects.Enums.MessageLogType.Exception);

        //        //RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
        //        //    {
        //        //    {"Message", $"Communication logging failed for customernumer {customernumber} - {ex.Message}"},
        //        //    {"Location", "Modstroem.dk -> RegisterCustomerFlow" },
        //        //    {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Exception).ToString()},
        //        //    });
        //    }
        //}
    }
}


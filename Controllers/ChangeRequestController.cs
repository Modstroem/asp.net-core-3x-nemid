﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System;
using DataTransferObjects.Communication;
using DataTransferObjects;
using LibraryModstroemLogs;
using Newtonsoft.Json;
using RequestHelperCore;
using static DataTransferObjects.Enums;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Reflection.Metadata;

namespace aspnetcore_oidc.Controllers
{
    public class ChangeRequestController : Controller
    {
        Models.AppSettings settings = new Models.AppSettings();

        [Authorize] 
        public IActionResult Index([FromQuery(Name = "kundenummer")] string kundenummer, [FromQuery(Name = "tidligeremail")] string tidligeremail, [FromQuery(Name = "tidligeretelefonnummer")] string tidligeretelefonnummer, [FromQuery(Name = "nymail")] string nymail, [FromQuery(Name = "nyttelefonnummer")] string nyttelefonnummer)
        {
            if (User.Identity.IsAuthenticated && HttpContext.Session != null)
            {
                var validatedCPRNumber = User.Claims.Where(x => x.Type == "gov:saml:attribute:CprNumberIdentifier").Select(x => x.Value).FirstOrDefault();
                SendMail(validatedCPRNumber, kundenummer, tidligeremail, nymail, tidligeretelefonnummer, nyttelefonnummer);
                Logout().GetAwaiter().GetResult();
                return Redirect(String.Format($"{settings.Config.Keys.ThanksFeedback}")); 
            }
            return null;
        }


        protected Boolean SendMail(string cpr, string cusLbnr, string oldMail, string newMail, string oldTlfNr, string newTlfNr)
        {
            try
            {
                string message = 
                "<br/>kundenummer: " + cusLbnr +
                "<br/>cpr nummer: " + cpr +
                "<br/>tidligere mail: " + oldMail +
                "<br/>ny mail: " + newMail +
                "<br/>tidligere telefonnummer: " + oldTlfNr +
                "<br/>nyt telefonnummer: " + newTlfNr;

                List<QueueItem> queueItems = new List<QueueItem>();
                    
                queueItems.Add(new QueueItem
                {
                    SenderID = 2,
                    AddedBy = "ChangeRequestFlow",
                    CreateTicket = true,
                    Instant = true,
                    customerNumber = Convert.ToInt32(cusLbnr),
                    Trackable = false,
                    documenttype = DataTransferObjects.Enums.DocumentType.SpotElPrices, //SpotElPrices is used as general template but is named SpotElPrices
                    documentFormat = Enums.DocumentFormat.HTML,
                    procesType = ProcessingType.Email,
                    additionelInfo = new AdditionelInfo
                    {
                        mergeData = new Dictionary<string, string>() 
                        {
                            { "[MESSAGE]", message},
                            { "[TIMESTAMP]", DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss")}
                        },
                            MailSubject = "Anmodning om ændring af stamddata",
                            OtherEmail = "secondline@modstroem.dk"
                    }
                });
                
               

                var content = new StringContent(JsonConvert.SerializeObject(queueItems).ToString(), Encoding.UTF8, "application/json");
                var response = RequestHelper.APIPost("NotificationController", "SetDocumentsInQueue", null, content);

                if (response.IsSuccessStatusCode)
                {
                    Logging.CreateMessageLog($"Succes to send Email to customerLBNR: {cusLbnr} with cpr: {cpr}", "MitID -> ChangeRequestFlow", (int)DataTransferObjects.Enums.MessageLogType.Besked);

                    return true;
                }
                else
                {
                    Logging.CreateMessageLog($"Failed to send Email {cusLbnr} with cpr: {cpr}", "MitID -> ChangeRequestFlow", (int)DataTransferObjects.Enums.MessageLogType.Fejl);

                    return false;
                }
            }
            catch (Exception ex)
            {
                Logging.CreateMessageLog($"Exception to send Email {cusLbnr} - {ex.Message} - {ex.StackTrace}", "MitID -> ChangeRequestFlow", (int)DataTransferObjects.Enums.MessageLogType.Exception);

                return false;
            }
        }

        public async Task Logout()
        {
            // Call the server to terminate the session
            await HttpContext.SignOutAsync(OpenIdConnectDefaults.AuthenticationScheme);
            // Remove authnetication cookies
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }
    }
}

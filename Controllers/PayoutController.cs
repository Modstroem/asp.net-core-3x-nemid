﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authentication;
using System.Threading.Tasks;
using EFS_DAL.Models;

namespace aspnetcore_oidc.Controllers
{
    public class PayoutController : Controller
    {
        Models.AppSettings settings = new Models.AppSettings();

        [Authorize]
        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated && HttpContext.Session != null)
            {
                var validatedCustomername = User.Claims.Any(x => x.Type.ToLower().StartsWith("name")) ? User.Claims.Where(x => x.Type == "name").Select(x => x.Value).FirstOrDefault() : String.Empty;
                var validatedCPRNumber = User.Claims.Where(x => x.Type == "gov:saml:attribute:CprNumberIdentifier").Select(x => x.Value).FirstOrDefault();

                Logout().GetAwaiter().GetResult();
                return Redirect(String.Format($"{settings.Config.Keys.PayoutFeedback}?validatedCustomername={validatedCustomername}&validatedCPRNumber={validatedCPRNumber}"));
            }
            return null;
        }

        public async Task Logout()
        {
            // Call the server to terminate the session
            await HttpContext.SignOutAsync(OpenIdConnectDefaults.AuthenticationScheme);
            // Remove authnetication cookies
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }
    }
}

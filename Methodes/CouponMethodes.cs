﻿using EFS_DAL.Models;
using ObjectFactoryCore.Partner;
using RequestHelperCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace aspnetcore_oidc.Methodes
{
    public class CouponMethodes
    {
        public static Tuple<string, string, int> GetCoupon(int partnerID)
        {
            try
            {
                string emailToRecieveError = "BFE@modstroem.dk";
                string PartnerName = string.Empty;
                string coupon = string.Empty;
                int SellerID = 0;

                CampaignCoupon availableCoupon;

                if (partnerID > 0)
                {
                    //The chosen partner details.
                    DefCampaignPartner mitIDPartner = CampaignPartner.GetPartner(partnerID);
                    SellerID = mitIDPartner.SellerId;
                    //Get the coupon/voucher of that partner
                    List<CampaignCoupon> mitIDCoupons = CampaignPartner.GetCoupon(mitIDPartner.PartnerId);

                    //Check for any available coupon and return the first one from the chosen partner.
                    if (mitIDPartner != null && mitIDCoupons != null && mitIDCoupons.Count > 0 && mitIDCoupons.Any(x => x.Available == 1 && x.CustomerLbnr == 0))
                    {
                        availableCoupon = mitIDCoupons.Where(x => x.Available == 1).First();

                        PartnerName = mitIDPartner.PartnerName;

                        var res = RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                     {
                        {"Message", $"Status: Ny kunde anvendte kupon fra kampagne: {PartnerName}, and amount coupon left: {mitIDCoupons.Where(x => x.Available == 1).Count() - 1}"},
                        {"Location", "Modstroem.dk -> CampaignFormFlow" },
                        {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.UserActivity).ToString()},
                     });

                        // Send Partner Coupon status to Besnik
                        GeneralMethodes.SendEmail(string.Format($"Status: Ny kunde anvendte kupon fra kampagne: {mitIDPartner.PartnerName}, and amount coupon left: {mitIDCoupons.Where(x => x.Available == 1).Count() - 1}"), emailToRecieveError, "Partner Campaign", "CampaignCoupon - Mit & NemID", "Partner Portal Status");

                        //sets the coupon to 0 (used)
                        CampaignPartner.UpdateCouponAvailable(availableCoupon, 0);

                        Tuple<string, string, int> couponTuple = new Tuple<string, string, int>(availableCoupon.Coupon, PartnerName, SellerID);

                        return couponTuple;
                    }
                    else
                    {
                        Tuple<string, string, int> couponTupleFail = new Tuple<string, string, int>(string.Empty, string.Empty, 0);

                        return couponTupleFail;
                    }
                }
                Tuple<string, string, int> noPartnerTuple = new Tuple<string, string, int>(string.Empty, string.Empty, 0);

                return noPartnerTuple;
            }
            catch (Exception ex)
            {
                var res = RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                     {
                        {"Message", $"Finding PartnerId failed. {ex.Message}"},
                        {"Location", "Modstroem.dk -> CampaignFormFlow" },
                        {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Exception).ToString()},
                     });

                throw;
            }

        }
    }
}

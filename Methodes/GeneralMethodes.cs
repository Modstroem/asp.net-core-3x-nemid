﻿using DataTransferObjects;
using DataTransferObjects.Communication;
using EFS_DAL.Models;
using LibraryModstroemLogs;
using Newtonsoft.Json;
using ObjectFactoryCore.Customer;
using RequestHelperCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using static DataTransferObjects.Enums;

namespace aspnetcore_oidc.Methodes
{
    public class GeneralMethodes
    {
        public static long GetCommunicationID(string origin_IP, DataTransferObjects.OrdreOprettelse.CustomerDTO customer, string name)
        {
            // Dont change this!! 
            const string changePage = "sideSkift";

            Models.AppSettings settings = new Models.AppSettings();
            DateTime currentDate = DateTime.Now;
            string formattedDate = currentDate.ToString("yyyy-MM-dd");

            string apiUrl = $"{settings.Config.Keys.MinSideApi}/Logging/CreateCommunicationPost?" +
                         $"method=1&type=25&time={formattedDate}&" +
                         $"direction=1&ip={origin_IP}&customerLbnr={customer.Kundenummer}&destination=RegisterCustomerFlow";


            StringBuilder builtString = new StringBuilder();

            builtString.Append(changePage);
            builtString.Append(addHTMLProfilePage(customer, origin_IP, name));
            string decodedHtml = builtString.ToString();
            string encodedHtml = Convert.ToBase64String(Encoding.UTF8.GetBytes(decodedHtml));
            long comID = 0;

            using (HttpClient client = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(encodedHtml), UnicodeEncoding.UTF8, "application/json");
                try
                {
                    var result = client.PostAsync(apiUrl, content).Result;
                    var responseContent = result.Content.ReadAsStringAsync().Result;

                    if (result.IsSuccessStatusCode)
                    {
                        long.TryParse(responseContent, out comID);

                        Logging.CreateMessageLog($"Success CommunicationID: {comID} url: {origin_IP}", "Mitid -> GeneralMethodes -> GetCommunicationID()", (int)Enums.MessageLogType.Besked);
                    }
                    else
                    {
                        Logging.CreateMessageLog($"Failed CommunicationID: {comID} at {DateTime.UtcNow}", "Mitid -> GeneralMethodes -> GetCommunicationID()", (int)Enums.MessageLogType.Fejl);
                    }
                }
                catch (Exception ex)
                {
                    Logging.CreateMessageLog($"GetCommunicationID ERROR at {DateTime.UtcNow} Message: {ex.InnerException.InnerException.Message}", "Mitid -> GeneralMethodes -> GetCommunicationID()", (int)Enums.MessageLogType.Exception);
                }
            }

            return comID;
        }
        public static void CreateLogMessage(string message, string location, int logType)
        {
            RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
            {
                {"Message", message},
                {"Location", location },
                {"LogType", logType.ToString()}
            });
        }
        /// <summary>
        /// Cleans the CPRNumber if it contains dash (-). 
        /// </summary>
        /// <param name="cprnumber"></param>
        /// <returns>Clean CPRNumber in string</returns>
        public static string RemoveCprDash(string cprnumber)
        {
            string cpr;
            if (cprnumber.Contains("-"))
            {
                cpr = cprnumber.Replace("-", "");
                return cpr;
            }
            return cprnumber;
        }
        /// <summary>
        /// Checks if the age of the customer is 18 or above.
        /// </summary>
        /// <param name="age">NemID</param>
        /// <param name="youth">MitID</param>
        /// <returns>true if the age is 18 or above</returns>
        public static bool ValidAge(int age, string youth)
        {
            if (age >= 18 || youth == "false")
            {
                //The age of 18 or above.
                return true;
            }
            else
            {
                //Under the age of 18.
                return false;
            }
        }

        /// <summary>
        /// Splits the fullname of the customer into firstname and lastname
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns>tuple<string,string> with the item1= firstname & item2= lastname</string></returns>
        public static Tuple<string, string> SplitFullName(string fullName)
        {
            string customerFirstName = string.Empty;
            string customerLastName = string.Empty;
            var names = fullName.Split(' ');
            customerFirstName = names.Length > 0 ? names.First() : String.Empty;
            customerLastName = fullName.Replace(customerFirstName, "").Trim();
            Tuple<string, string> name = new Tuple<string, string>(customerFirstName,customerLastName);
            return name;
            
        }
        /// <summary>
        /// Sends email with a message from the parameter.
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="email"></param>
        /// <returns>true for succes</returns>
        public static Boolean SendEmail(string Message, string email, string addedBy, string location, string mailSubject)
        {
            try
            {
                List<QueueItem> queueList = new();
                //AppSettings appSettings = new AppSettings();
                queueList.Add(new QueueItem
                {
                    SenderID = 3,
                    AddedBy = addedBy,
                    Instant = true,
                    customerNumber = 331931,
                    Trackable = false,
                    documenttype = DocumentType.SpotElPrices,
                    documentFormat = Enums.DocumentFormat.HTML,
                    procesType = ProcessingType.Email,
                    additionelInfo = new AdditionelInfo
                    {

                        mergeData = new Dictionary<string, string>() {
                    { "[MESSAGE]", Message},
                    { "[TIMESTAMP]", DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss")}
                    },
                        MailSubject = mailSubject,//"Partner Portal Status"
                        OtherEmail = email
                    }
                });

                String serializedItems = JsonConvert.SerializeObject(queueList).ToString();
                var content = new StringContent(serializedItems, Encoding.UTF8, "application/json");

                var response = RequestHelperCore.RequestHelper.APIPost("NotificationController", "SetDocumentsInQueue", null, content);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Responce res = JsonConvert.DeserializeObject<Responce>(response.Content.ReadAsStringAsync().Result);

                    Logging.CreateMessageLog($"Adding mail to queue succeded {res}", "RegisterCustomerFlow -> Modstroem.dk", (int)DataTransferObjects.Enums.MessageLogType.Besked);

                    //RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                    // {
                    //    {"Message", $"Adding mail to queue succeded {res}"},
                    //    {"Location", location },//"CampaignCoupon - Mit&NemID"
                    //    {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Besked).ToString()},
                    // });
                    //MessageLog.CreateLogEntry("Adding mail to queue succeded - ", "CampaignCoupon", Enums.MessageLogType.Besked);

                    return true;
                }
                else
                {
                    string msg = response.Content.ReadAsStringAsync().Result;

                    Logging.CreateMessageLog($"Adding mail to queue failed {msg}", "RegisterCustomerFlow -> Modstroem.dk", (int)DataTransferObjects.Enums.MessageLogType.Fejl);

                    //RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                    // {
                    //    {"Message", $"Adding mail to queue failed {msg}"},
                    //    {"Location", location },
                    //    {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Fejl).ToString()},
                    // });
                    //MessageLog.CreateLogEntry("Adding mail to queue failed - ", "CampaignCoupon", Enums.MessageLogType.Fejl);

                    return false;
                }
            }
            catch (Exception ex)
            {
                Logging.CreateMessageLog($"Sending Email failed {ex.Message}", "RegisterCustomerFlow -> Modstroem.dk", (int)DataTransferObjects.Enums.MessageLogType.Exception);

                //RequestHelper.APIRequest("CommunicationController", "CreateLogMessage", new Dictionary<string, string>
                //{
                //    {"Message", $"Sending Email failed {ex.Message}"},
                //    {"Location", "CampaignCoupon - Mit&NemID" },
                //    {"LogType", ((int)DataTransferObjects.Enums.MessageLogType.Exception).ToString()},
                //});
                //LoggingLibrary.MessageLog.CreateLogEntry($"Sending Email failed. {ex.Message}", "Modstroem.dk -> GetCoupon/MitID", DataTransferObjects.Enums.MessageLogType.Exception);
                throw;
            }
        }

        static String addHTMLProfilePage(DataTransferObjects.OrdreOprettelse.CustomerDTO custObj, String IP, string mitIDName)
        {
            String tableString = String.Empty;
            String betalingsMetode = String.Empty;
            String cprNumber = String.Empty;

            if (custObj.BetalingsID == (int)DataTransferObjects.Enums.PaymentType.PBS)
            {
                betalingsMetode = "PBS";
            }
            else
            {
                betalingsMetode = "Giro indbetaling";
            }

            tableString = "<table style='width:500px;'>";
            tableString += "<tr style='height:30px;'>";
            tableString += "<th colspan=2 align='center'>Kundeprofil</th>";
            tableString += "</tr>";

            tableString += addHTMLTableRow("Sælger", "Modstrøm Danmark A/S");
            tableString += addHTMLTableRow("Kontaktperson", custObj.produkter.FirstOrDefault().Sælger.ToString());
            tableString += addHTMLTableRow("Oprettet", DateTime.Now.ToString("G"));
            tableString += addHTMLTableRow("Kundenavn", custObj.Fornavn + " " + custObj.Efternavn);

            if (custObj.CPRNummer.Length > 6 && custObj.CPRNummer != String.Empty)
                cprNumber = String.Format("{0}-XXXX", custObj.CPRNummer.Substring(0, 6));

            tableString += addHTMLTableRow("CPR", cprNumber);
            tableString += addHTMLTableRow("Kontaktperson", custObj.Fornavn + " " + custObj.Efternavn);
            tableString += addHTMLTableRow("Adresse", custObj.Gadenavn + " " + custObj.Husnummer);
            tableString += addHTMLTableRow("Postnr & by", custObj.Postnummer + " " + custObj.Bynavn);
            tableString += addHTMLTableRow("Emailadresse", custObj.Email);
            tableString += addHTMLTableRow("Telefonnummer", custObj.Telefon);
            tableString += addHTMLTableRow("Mobilnummer", custObj.Telefon);
            tableString += addHTMLTableRow("Betalingsmetode", betalingsMetode);
            tableString += addHTMLTableRow("Registreringsnummer", custObj.Registreringsnummer == "0000" ? string.Empty : custObj.Registreringsnummer);
            tableString += addHTMLTableRow("Kontonummer", custObj.Kontonummer == "000000" ? string.Empty : custObj.Kontonummer);
            tableString += addHTMLTableRow("Udsendelsesmetode", "Mail");
            tableString += addHTMLTableRow("Bekræftelsesmetode", "Link");
            tableString += addHTMLTableRow("Bekræftelsesdato", DateTime.Now.ToString("G"));
            tableString += addHTMLTableRow("Bekræftet af", IP);
            tableString += addHTMLTableRow("Bekræftet af via MitID", mitIDName);
            tableString += "</table>";

            return tableString;

        }

        static String addHTMLTableRow(String FieldName, String value)
        {
            String rowString = String.Empty;
            rowString += "<tr>";
            rowString += String.Format("<td style='width:150px;height: 16px; font-family:Verdana;font-weight:bold'>{0}</td>", FieldName);
            rowString += "<td style='font-family:Verdana; font-weight:normal;'>" + value + "</td>";
            rowString += "</tr>";

            return rowString;
        }
    }
}
